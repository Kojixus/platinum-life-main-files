
class CfgPatches {
	class AL_HWP_Clothing {
		units[] = {"B_Competitor_F"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Characters_F"};
	};
};

class CfgVehicles {
	class B_Competitor_F;	// External class reference
		class AL_HWP_Officer : B_Competitor_F {
			_generalMacro = "B_Competitor_F";
			scope = 2;
			nakedUniform = "U_BasicBody";
			uniformClass = "AL_PD_Officer_Uniform";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"AL_HWP_Clothing\Data\AL_HWP_Trooper.paa"};
			displayName = "AL HWP Officer Uniform";
			author = "Arma-Life";
	};
		class AL_HWP_SnrOfficer : B_Competitor_F {
			_generalMacro = "B_Competitor_F";
			scope = 2;
			nakedUniform = "U_BasicBody";
			uniformClass = "AL_PD_Corporal_Uniform";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"AL_HWP_Clothing\Data\AL_HWP_SnrOfficer.paa"};
			displayName = "AL HWP Corporal Uniform";
			author = "Arma-Life";
	};
	
		class AL_HWP_Corporal : B_Competitor_F {
			_generalMacro = "B_Competitor_F";
			scope = 2;
			nakedUniform = "U_BasicBody";
			uniformClass = "AL_PD_Sergeant_Uniform";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"AL_HWP_Clothing\Data\AL_HWP_Corporal.paa"};
			displayName = "AL HWP Corporal Uniform";
			author = "Arma-Life";
	};
	
		class AL_HWP_Sergeant : B_Competitor_F {
			_generalMacro = "B_Competitor_F";
			scope = 2;
			nakedUniform = "U_BasicBody";
			uniformClass = "AL_PD_Sergeant_Uniform";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"AL_HWP_Clothing\Data\AL_HWP_Sergeant.paa"};
			displayName = "AL HWP Sergeant Uniform";
			author = "Arma-Life";
	};
	
		class AL_HWP_Lieutenant : B_Competitor_F {
			_generalMacro = "B_Competitor_F";
			scope = 2;
			nakedUniform = "U_BasicBody";
			uniformClass = "AL_PD_Lieutenant_Uniform";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"AL_HWP_Clothing\Data\AL_HWP_Lieutenant.paa"};
			displayName = "AL HWP Lieutenant Uniform";
			author = "Arma-Life";
	};
		class AL_HWP_Captain : B_Competitor_F {
			_generalMacro = "B_Competitor_F";
			scope = 2;
			nakedUniform = "U_BasicBody";
			uniformClass = "AL_PD_Captain_Uniform";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"AL_HWP_Clothing\Data\AL_HWP_Captain.paa"};
			displayName = "AL HWP Captain Uniform";
			author = "Arma-Life";
	};

};

class cfgWeapons {
	class Uniform_Base;	// External class reference
	class UniformItem;	// External class reference

	class AL_PD_Officer_Uniform : Uniform_Base {
		scope = 2;
		displayName = "AL HWP Officer Uniform";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		author = "Arma-Life";
		
		class ItemInfo : UniformItem {
			uniformModel = "-";
			uniformClass = "AL_HWP_Officer";
			containerClass = "Supply100";
			mass = 2;
		};
	};
	class AL_PD_SnrOfficer_Uniform : Uniform_Base {
		scope = 2;
		displayName = "AL HWP SnrOfficer Uniform";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		author = "Arma-Life";
		
		class ItemInfo : UniformItem {
			uniformModel = "-";
			uniformClass = "AL_HWP_SnrOfficer";
			containerClass = "Supply100";
			mass = 2;
		};
	};
	class AL_PD_Corporal_Uniform : Uniform_Base {
		scope = 2;
		displayName = "AL HWP Corporal Uniform";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		author = "Arma-Life";
		
		class ItemInfo : UniformItem {
			uniformModel = "-";
			uniformClass = "AL_HWP_Corporal";
			containerClass = "Supply100";
			mass = 2;
		};
	};
	class AL_PD_Sergeant_Uniform : Uniform_Base {
		scope = 2;
		displayName = "AL HWP Sergeant Uniform";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		author = "Arma-Life";
		
		class ItemInfo : UniformItem {
			uniformModel = "-";
			uniformClass = "AL_HWP_Sergeant";
			containerClass = "Supply100";
			mass = 2;
		};
	};
	class AL_PD_Lieutenant_Uniform : Uniform_Base {
		scope = 2;
		displayName = "AL HWP Lieutenant Uniform";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		author = "Arma-Life";
		
		class ItemInfo : UniformItem {
			uniformModel = "-";
			uniformClass = "AL_HWP_Lieutenant";
			containerClass = "Supply100";
			mass = 2;
		};
	};
	class AL_PD_Captain_Uniform : Uniform_Base {
		scope = 2;
		displayName = "AL HWP Captain Uniform";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		author = "Arma-Life";
		
		class ItemInfo : UniformItem {
			uniformModel = "-";
			uniformClass = "AL_HWP_Captain";
			containerClass = "Supply100";
			mass = 2;
		};
	};
};