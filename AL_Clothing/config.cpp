#include "BIS_AddonInfo.hpp"
class DefaultEventhandlers;
class CfgPatches
{
class AL_Clothing
{
units[] = 
{
	"travis_polocookiemonster_rangemaster",
	"kiiika_fan_rangemaster",
	"fireuniform_soldier",
	"travis_polofucka3pl_rangemaster",
	"travis_polopsisyndicate_rangemaster",
	"travis_polosecurity_rangemaster",
	"travis_poloapple_rangemaster",
	"travis_polohellokitty_rangemaster",
	"travis_polohellokitty1_rangemaster",
	"travis_poloaustralia_rangemaster",
	"travis_polospongebob_rangemaster",
	"travis_polopatrickstar_rangemaster",
	"travis_polocanda_rangemaster",
	"travis_pololove_rangemaster",
	"travis_polofuck_rangemaster",
	"travis_polococacola_rangemaster",
	"travis_poloangelo_rangemaster",
	"travis_polomrbong_rangemaster",
	"travis_polomoon_rangemaster",
	"travis_polodino_rangemaster",
	"travis_poloface_rangemaster",
	"travis_polostunt_rangemaster",
	"travis_avengers_rangemaster",
	"travis_doge_rangemaster",
	"travis_fuckit_rangemaster",
	"travis_illuminati_rangemaster",
	"travis_jeff_rangemaster",
	"travis_kappa_rangemaster",
	"travis_married_rangemaster",
	"travis_marvel_rangemaster",
	"travis_mexicantroll_rangemaster","travis_minion_rangemaster","travis_monstercat_rangemaster","travis_nike_rangemaster","travis_pancake_rangemaster","travis_shield_rangemaster","travis_smile_rangemaster","travis_timmac_rangemaster","travis_tux_rangemaster","travis_twitch_rangemaster","travis_yaoming_rangemaster","travis_Butts_rangemaster","travis_Snoopy_rangemaster","travis_Coco_rangemaster","travis_Batman_rangemaster","kojixus_spawnclothing_rangemaster","kojixus_vapenation_rangemaster","kojixus_supreme_rangemaster","kojixus_treyway_rangemaster","kojixus_wizard_rangemaster","kojixus_psi_rangemaster","kojixus_psiblack_rangemaster","kojixus_xxxtenaction_rangemaster","kojixus_trippieredd_rangemaster","kojixus_mexicanfarmer_rangemaster","kojixus_angrypepe_rangemaster","kojixus_russianpepe_rangemaster","kojixus_picklerick_rangemaster","kojixus_wolf_rangemaster","travis_ciggy_rangemaster","travis_Adidas_rangemaster","travis_weed_rangemaster","travis_ReclaimJoey_rangemaster","travis_Feature_rangemaster","casper_beautifulbackpack","casper_cockbackpack","casper_facbackpack","casper_hellokittybackpack","casper_mcdonaldsbackpack","casper_nemobackpack","casper_ppatrolbackpack","travis_InvisibeCarryall","travis_InmateCounty_rangemaster","travis_InmatePrison_rangemaster","AL_MillersUniform_Soldier","AL_SyndicateUniform_Soldier","AL_YakuzaUniform_Soldier","AL_ManarelloUniform_Soldier","AL_UrFatUniform_Soldier","travis_emstraineeuniform_soldier","travis_emsemtuniform_soldier","travis_emsparamedicuniform_soldier","travis_emsseniorparamedicuniform_soldier","travis_emscommanduniform_soldier","AL_PoliceWetsuit_Diver","AL_Police0_RangeMaster","AL_Police1_RangeMaster","AL_Police2_RangeMaster","AL_Police3_RangeMaster","AL_Police4_RangeMaster","AL_Police5_RangeMaster","AL_Police6_RangeMaster","AL_Police16_RangeMaster","AL_Police17_RangeMaster","AL_Police18_RangeMaster","AL_Police19_RangeMaster","AL_Police20_RangeMaster","AL_SWAT3_RangeMaster","AL_SWAT4_RangeMaster","AL_SWAT5_RangeMaster","AL_SWAT6_RangeMaster","AL_Corrections1_RangeMaster","AL_Corrections2_RangeMaster","AL_Corrections3_RangeMaster","AL_Corrections4_RangeMaster","AL_Corrections5_RangeMaster","AL_Corrections6_RangeMaster","AL_Trooper1_RangeMaster","AL_Trooper2_RangeMaster","AL_Trooper3_RangeMaster","AL_Trooper4_RangeMaster","AL_Trooper5_RangeMaster","AL_Trooper6_RangeMaster","AL_HWP1_RangeMaster","AL_HWP2_RangeMaster","AL_HWP3_RangeMaster","AL_HWP4_RangeMaster","AL_HWP5_RangeMaster","AL_CRTUniform_Soldier","AL_SWATUniform_Soldier","AL_IAUniform_RangeMaster","AL_Tactical1_Soldier","AL_Tactical2_Soldier","AL_Tactical3_Soldier","AL_FTO1_Rangemaster","AL_FTO2_Rangemaster","AL_FTO3_Rangemaster","AL_FTO4_Rangemaster","AL_FTO5_Rangemaster","AL_RepriisalUniform_Soldier","AL_BratvaUniform_Soldier","AL_ffUniform_Soldier","AL_OutlawUniform_Soldier","AL_ChauUniform_Soldier","AL_VigilantesUniform_Soldier,""al_devilshenchmenuniform_soldier","AL_atlasUniform_Soldier"};
weapons[] = {"travis_polocookiemonster","kiiika_fan","Finiclothes","fireuniform","travis_polopsisyndicate","travis_polosecurity","travis_poloapple","travis_polohellokitty","travis_polohellokitty1","travis_poloaustralia","travis_Batman","travis_smokeweed","travis_polospongebob","travis_polopatrickstar","travis_polocanda","travis_pololove","travis_polofuck","travis_polococacola","travis_poloangelo","travis_polomrbong","travis_polomoon","travis_polodino","travis_polofucka3pl","travis_polostunt","travis_poloface","travis_avengers","travis_doge","travis_fuckit","travis_illuminati","travis_jeff","travis_kappa","travis_married","travis_marvel","travis_mexicantroll","travis_minion","travis_monstercat","travis_nike","travis_pancake","travis_shield","travis_smile","travis_timmac","travis_tux","travis_twitch","travis_yaoming","travis_Butts","travis_Snoopy","travis_Coco","travis_Ciggy","travis_Adidas","travis_ReclaimJoey","kojixus_spawnclothing","kojixus_supreme","kojixus_vapenation","kojixus_treyway","kojixus_wizard","kojixus_psi","kojixus_psiblack","kojixus_mexicanfarmer","kojixus_trippieredd","kojixus_angrypepe","kojixus_russianpepe","kojixus_picklerick","kojixus_wolf","kojixus_xxxtenaction","travis_Feature","travis_emstraineeuniform","travis_emsemtuniform","travis_emsparamedicuniform","travis_emsseniorparamedicuniform","travis_emscommanduniform","travis_TraineeVest","travis_EMTVest","travis_RideAlongVest","travis_CommandVest","travis_ParamedicVest","FirstResponder_vest","travis_SARVest","travis_InmateCounty","travis_InmatePrison","AL_DOJVest","AL_DOJVestCommand","AL_SWATHeavyVest","AL_InternalAffairsHeavyVest","AL_CRTHeavyVest","AL_PoliceHeavyVest","AL_TrooperHeavyVest","AL_CorrectionsHeavyVest","AL_CIDHeavyVest","AL_CIDHeavyGreenVest","AL_CERTHeavyVest","AL_HWPHeavyVest","AL_CartelHeavyVest","AL_MafiaHeavyVest","AL_CivHeavyVest1","AL_CivHeavyVest2","AL_CivHeavyVest3","AL_CivHeavyVest4","AL_CivHeavyVest5","AL_TacticALHelmet","AL_LosDCartelVest","AL_BandidosVest","AL_VigilantesUniform","AL_VigilantesVest","AL_SosaVest","AL_SonoraVest","AL_ReddingtonVest","AL_BrotherhoodUniform","AL_BrotherhoodVest","AL_MillersUniform","AL_MillersVest","AL_SyndicateUniform","AL_SyndicateVest","AL_atlasvest","AL_YakuzaUniform","AL_YakuzaVest","AL_ManarelloUniform","AL_ManarelloVest","AL_UrFatUniform","AL_UrFatVest","AL_PoliceRebreatherHeavyVest","AL_PoliceWetsuit","AL_Police0","AL_Police1","AL_Police2","AL_Police3","AL_Police4","AL_Police5","AL_Police6","AL_Police16","AL_Police17","AL_Police18","AL_Police19","AL_Police20","AL_FTO1","AL_FTO2","AL_FTO3","AL_FTO4","AL_FTO5","AL_SWAT2","AL_SWAT3","AL_SWAT4","AL_SWAT5","AL_SWAT6","AL_Corrections1","AL_Corrections2","AL_Corrections3","AL_Corrections4","AL_Corrections5","AL_Corrections6","AL_Trooper1","AL_Trooper2","AL_Trooper3","AL_Trooper4","AL_Trooper5","AL_Trooper6","AL_HWP1","AL_HWP2","AL_HWP3","AL_HWP4","AL_HWP5","AL_CRTUniform","AL_SWATUniform","AL_IAUniform","AL_Tactical1","AL_Tactical2","AL_Tactical3","AL_BOIJacketBlack","AL_BOIJacketBlue","AL_BOIJacketPurple","AL_BOIBeret","AL_BOI_Beanie_Black","AL_BOI_Beanie_Blue","AL_BOI_Beanie_Purple","AL_ReprisalVest","AL_bratvavest","AL_ffvest","AL_ReprisalUniform","AL_BratvaUniform","AL_ffUniform","AL_OutlawUniform","AL_OutlawVest","AL_ChauVest","AL_ChauUniform","al_devilshenchmenvest","al_devilshenchmenuniform","AL_PeakyBlindersVest","AL_atlasUniform","AL_atlas_vest"};
requiredVersion = 0.1;
requiredAddons[] = {"A3_Characters_F","A3_Characters_F_BLUFOR","A3_Characters_F_Common","A3_Weapons_F","A3_Characters_F_Beta_INDEP"};
};
};
class CfgVehicles
{
class B_RangeMaster_F;
class b_soldier_03_f;
class b_soldier_survival_F;
class I_soldier_F;
class B_soldier_F;
class B_Soldier_diver_base_F;
class B_Competitor_F;
class B_Carryall_base;
class UniformSlotInfo;
class VestItem;
class Man;
class B_Soldier_base_F;
class B_Carryall_oli;
class CAManBase: Man
{
class HitPoints
{
class HitHead;
class HitBody;
class HitHands;
class HitLegs;
};
};
class travis_InmateCounty_rangemaster: B_Soldier_base_F
{
_generalMacro = "B_Soldier_base_F";
scope = 2;
model = "tryk_unit\data\Nomex_zabb.p3d";
uniformClass = "travis_InmateCounty";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\travisbutts_countyinmate.paa"};
displayName = "[PL] Inmate Jumpsuit - County";
};
class travis_InmatePrison_rangemaster: B_Soldier_base_F
{
_generalMacro = "B_Soldier_base_F";
scope = 2;
model = "tryk_unit\data\Nomex_zabb.p3d";
uniformClass = "travis_InmatePrison";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\travisbutts_prisoninmate.paa"};
displayName = "[PL] Inmate Jumpsuit - Prison";
};
class travis_polocookiemonster_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Cookie Monster Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polocookiemonster";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Monster.paa"};
};
class kiiika_fan_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Kiiika Fan";
nakedUniform = "U_BasicBody";
uniformClass = "kiiika_fan";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kiiika_fan.paa"};
};
class Finiclothes_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Fini's Clothes";
nakedUniform = "U_BasicBody";
uniformClass = "Finiclothes";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\Finiclothes.paa"};
};
class travis_polofucka3pl_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Fuck A3PL Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polofucka3pl";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_FuckA3PL.paa"};
};
class travis_polopsisyndicate_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] PsiSyndicate Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polopsisyndicate";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_PsiSyndicate.paa"};
};
class travis_polosecurity_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Security Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polosecurity";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Security.paa"};
};
class travis_poloapple_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Apple Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_poloapple";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_AppleTech.paa"};
};
class travis_polohellokitty_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Hello Kitty Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polohellokitty";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_HelloKitty.paa"};
};
class travis_polohellokitty1_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Hello Kitty 2 Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polohellokitty1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_HelloKitty1.paa"};
};
class travis_poloaustralia_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Australia Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_poloaustralia";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Australia.paa"};
};
class travis_polospongebob_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Spongebob Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polospongebob";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Spongebob.paa"};
};
class travis_polopatrickstar_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Patrick StarPolo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polopatrickstar";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Patrick.paa"};
};
class travis_polocanda_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Canda Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polocanda";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Canada.paa"};
};
class travis_pololove_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] I Love Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_pololove";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Love.paa"};
};
class travis_polofuck_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Fuck The Police Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polofuck";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_FuckThePolice.paa"};
};
class travis_polococacola_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Coca Cola Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polococacola";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Cocacola.paa"};
};
class travis_poloangelo_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Michel Angelo Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_poloangelo";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Angelo.paa"};
};
class travis_polomrbong_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Mr Bong Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polomrbong";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_MrBong.paa"};
};
class travis_polomoon_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Pink Floyd Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polomoon";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Moon.paa"};
};
class travis_polodino_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Dinosaur Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polodino";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Dino.paa"};
};
class travis_poloface_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Face Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_poloface";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Face.paa"};
};
class travis_polostunt_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Stunt Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_polostunt";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Stunt.paa"};
};
class travis_avengers_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Avengers Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_avengers";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Avengers.paa"};
};
class travis_doge_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Doge Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_doge";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Doge.paa"};
};
class travis_fuckit_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Fuck It Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_fuckit";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Fuckit.paa"};
};
class travis_illuminati_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Illuminati Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_illuminati";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Illuminati2016.paa"};
};
class travis_jeff_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Jeff Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_jeff";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Jeff.paa"};
};
class travis_kappa_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Kappa Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_kappa";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Kappa.paa"};
};
class travis_married_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Married Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_married";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Married.paa"};
};
class travis_marvel_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Marvel Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_marvel";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Marvel.paa"};
};
class travis_mexicantroll_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Mexican Troll Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_mexicantroll";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_MexicanTroll.paa"};
};
class travis_minion_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Minion Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_minion";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Minion.paa"};
};
class travis_monstercat_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Monstercat Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_monstercat";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Monstercat.paa"};
};
class travis_nike_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Nike Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_nike";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Nike.paa"};
};
class travis_pancake_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Pancake Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_pancake";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Pancake.paa"};
};
class travis_shield_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] S.H.I.E.L.D. Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_shield";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Shield.paa"};
};
class travis_smile_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Smile Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_smile";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Smile.paa"};
};
class travis_timmac_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Timmac Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_timmac";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Timmac.paa"};
};
class travis_tux_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Tux Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_tux";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Tux.paa"};
};
class travis_twitch_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Twitch Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_twitch";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Twitch.paa"};
};
class travis_yaoming_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Yao Ming Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_yaoming";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Yaoming.paa"};
};
class travis_Butts_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Kiika Butts Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_butts";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Butts.paa"};
};
class travis_Snoopy_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Snoopy Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_Snoopy";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Snoopy.paa"};
};
class travis_Coco_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Coco Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_Coco";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_EnjoyCocaine.paa"};
};
class travis_Batman_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Batman Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_Batman";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Batman.paa"};
};
class travis_ciggy_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Ciggy Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_ciggy";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Ciggy.paa"};
};
class travis_Adidas_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Adidas Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_Adidas";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Adidas.paa"};
};
class travis_weed_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Smoke Weed Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_smokeweed";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_SmokeWeedEverday.paa"};
};
class AL_BrotherhoodUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Brotherhood Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_BrotherhoodUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\brotherhooduni.paa"};
};
class AL_ChauUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Chau Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_ChauUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\CHAU3UNI.paa"};
};
class AL_MillersUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Millers Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_MillersUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\millersuni.paa"};
};
class AL_SyndicateUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] The Syndicate Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_SyndicateUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\syndicateuni.paa"};
};
class AL_atlasUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Atlas Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_atlasUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\atlasuni.paa"};
};
class AL_YakuzaUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Yakuza Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_YakuzaUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\yakuzauni.paa"};
};
class AL_ManarelloUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Manarello Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_ManarelloUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\manarellouni.paa"};
};
class AL_UrFatUniform_Soldier: b_soldier_survival_F
{
_generalMacro = "b_soldier_survival_F";
scope = 2;
nakedUniform = "U_BasicBody";
uniformClass = "AL_UrFatUniform";
model = "\A3\characters_F\BLUFOR\b_soldier_02.p3d";
modelClass = "AL_UrFatUniform";
hiddenSelections[] = {"camo","camo2"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\croftuni.paa","AL_Clothing\data\gangs\croftuniblank.paa"};
hiddenUnderwaterSelections[] = {"hide"};
shownUnderwaterSelections[] = {"unhide","unhide2"};
hiddenUnderwaterSelectionsTextures[] = {"\A3\characters_f\common\data\diver_equip_nato_co.paa","\A3\characters_f\common\data\diver_equip_nato_co.paa","\A3\characters_f\data\visors_ca.paa"};
displayName = "[PL] Ur Fat Gang Uniform";
};
class AL_ReprisalUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Reprisal Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_reprisalUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\reprisaluni.paa"};
};
class AL_BratvaUniform_Soldier: B_Soldier_base_F
{
        scope = 1;
        displayName = "[PL] Bratva Gang Uniform";
        model = "\tryk_unit\data\PCUs_G.p3d";
        hiddenSelections[] = {"camo","camo3"};
        hiddenSelectionsTextures[] = {"tryk_unit\data\fr\jeans_co.paa","AL_Clothing\data\gangs\bratvauni.paa"};
        hiddenSelectionsMaterials[] = {};
    };
class AL_VigilantesUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Vigilantes Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_ffUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\vigilclo.paa"};
};
class AL_OutlawUniform_Soldier: B_Soldier_base_F
{
        scope = 1;
        displayName = "[PL] Outlaw Gang Uniform";
        model = "\tryk_unit\data\PCUs_G.p3d";
        hiddenSelections[] = {"camo","camo3"};
        hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\outlawsuni1.paa","AL_Clothing\data\gangs\outlawsuni2.paa"};
        hiddenSelectionsMaterials[] = {};
    };
class AL_ffUniform_Soldier: I_soldier_F
{
_generalMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] Filthy Fraggers Gang Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_ffUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\ffuni.paa"};
};
class travis_ReclaimJoey_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Reclaim Joey Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_ReclaimJoey";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_ReclaimJoey.paa"};
};
class kojixus_spawnclothing_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Arma-Life Beta Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_spawnclothing";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_spawnclothing.paa"};
};
class kojixus_xxxtenaction_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] XXXtenaction Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_xxx";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_xxxtenaction.paa"};
};
class kojixus_psi_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] PsiSyn v2 Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_psi";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_psi.paa"};
};
class kojixus_psiblack_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] PsiSyn v1 Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_psiblack";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_psiblack.paa"};
};
class kojixus_wizard_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Wizard Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_wizard";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_wizard.paa"};
};
class kojixus_treyway_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] TREYWAY Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_treyway";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_treyway.paa"};
};
class kojixus_supreme_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Supreme Clothing";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_supreme";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_supreme.paa"};
};
class kojixus_vapenation_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Vape Nation";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_vapenation";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_vapenation.paa"};
};
class kojixus_wolf_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Wolf Shirt";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_wolf";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_wolf.paa"};
};
class kojixus_picklerick_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Pickle-Rick Shirt";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_picklerick";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_picklerick.paa"};
};
class kojixus_russianpepe_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Russian Pepe Shirt";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_russianpepe";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_russianpepe.paa"};
};
class kojixus_angrypepe_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Angry Pepe Shirt";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_angrypepe";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_angrypepe.paa"};
};
class kojixus_trippieredd_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Trippie-Redd Shirt";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_trippieredd";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_trippieredd.paa"};
};
class kojixus_mexicanfarmer_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Mexican Farmer Shirt";
nakedUniform = "U_BasicBody";
uniformClass = "kojixus_mexicanfarmer";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\kojixus_mexicanfarmer.paa"};
};
class travis_Feature_rangemaster: B_RangeMaster_F
{
_generalMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] FeatureTV Polo";
nakedUniform = "U_BasicBody";
uniformClass = "travis_Feature";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\TravisButts_Feature.paa"};
};
class travis_emstraineeuniform_soldier: B_soldier_F
{
_generalMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] EMS Trainee Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "travis_emstraineeuniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\White_and_White_Paramedic.paa"};
};
class travis_emsemtuniform_soldier: B_soldier_F
{
_generalMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] EMS EMT Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "travis_emsemtuniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\White_and_Blue_Paramedic.paa"};
};
class fireuniform_soldier: B_soldier_F
{
_generalMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] Fire and Rescue Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "fireuniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\fireclothes.paa"};
};
class travis_emsparamedicuniform_soldier: B_soldier_F
{
_generalMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] EMS Paramedic Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "travis_emsparamedicuniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Orange_and_White_and_Black.paa"};
};
class travis_emsseniorparamedicuniform_soldier: B_soldier_F
{
_generalMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] EMS Senior Paramedic Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "travis_emsseniorparamedicuniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Red_and_Black_Paramedic.paa"};
};
class travis_emscommanduniform_soldier: B_soldier_F
{
_generalMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] EMS Command Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "travis_emscommanduniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\White_and_Black_and_Blue_Paramedic.paa"};
};
class AL_PoliceWetsuit_Diver: B_Soldier_diver_base_F
{
_generalMacro = "B_diver_F";
scope = 2;
displayName = "[PL] Police Wetsuit";
model = "\A3\characters_f\Common\diver_slotable.p3d";
uniformClass = "AL_PoliceWetsuit";
hiddenSelections[] = {"Camo1","Camo2","insignia"};
hiddenSelectionsTextures[] = {"\AL_Clothing\data\police\uniforms\pwetsuit.paa","\AL_Clothing\data\police\uniforms\prebreather.paa"};
class UniformInfo
{
class SlotsInfo
{
class NVG: UniformSlotInfo
{
slotType = 602;
};
class Scuba: UniformSlotInfo
{
slotType = 604;
};
class Headgear: UniformSlotInfo
{
slotType = 605;
};
};
};
hiddenUnderwaterSelections[] = {"hide"};
shownUnderwaterSelections[] = {"unhide","unhide2"};
hiddenUnderwaterSelectionsTextures[] = {"\A3\characters_f\common\data\diver_equip_nato_co.paa","\A3\characters_f\common\data\diver_equip_nato_co.paa","\A3\characters_f\data\visors_ca.paa"};
};
class AL_Police0_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Cadet";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police0";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Cadet.paa"};
};
class AL_Police1_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Officer";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Officer.paa"};
};
class AL_Police2_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Senior Officer";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Senior.paa"};
};
class AL_Police3_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Corporal";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police3";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Corporal.paa"};
};
class AL_Police4_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Sergeant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police4";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Sergeant.paa"};
};
class AL_Police5_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Lieutenant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police5";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Lieutenant.paa"};
};
class AL_Police6_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Captain";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police6";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Captain.paa"};
};
class AL_Police16_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Inspector";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police16";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Inspector.paa"};
};
class AL_Police17_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Superintendent";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police17";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Superintendent.paa"};
};
class AL_Police18_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Assistant Chef";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police18";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\AssChief.paa"};
};
class AL_Police19_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Chef";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police19";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Chief.paa"};
};
class AL_Police20_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Police Commissioner";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Police20";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\Commissioner.paa"};
};
class AL_SWAT2_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
model = "\tryk_unit\data\PCUs_G.p3d";
displayName = "[PL] S.W.A.T. Patrol Uniform";
uniformClass = "AL_SWAT2";
hiddenSelections[] = {"camo","camo3"};
hiddenSelectionsTextures[] = {"tryk_unit\data\fr\jeans_co.paa","AL_Clothing\data\police\uniforms\flecktarn_Grey_co.paa"};
};
class AL_SWAT3_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] S.W.A.T. Corporal";
nakedUniform = "U_BasicBody";
uniformClass = "AL_SWAT3";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Corporal.paa"};
};
class AL_SWAT4_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] S.W.A.T. Sergeant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_SWAT4";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Sergeant.paa"};
};
class AL_SWAT5_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] S.W.A.T. Lieutenant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_SWAT5";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Lieutenant.paa"};
};
class AL_SWAT6_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] S.W.A.T. Captain";
nakedUniform = "U_BasicBody";
uniformClass = "AL_SWAT6";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Captain.paa"};
};
class AL_Corrections1_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Corrections Officer";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Corrections1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\C_Officer.paa"};
};
class AL_Corrections2_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Corrections Senior Officer";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Corrections2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\C_Senior.paa"};
};
class AL_Corrections3_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Corrections Corporal";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Corrections3";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\C_Corporal.paa"};
};
class AL_Corrections4_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Corrections Sergeant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Corrections4";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\C_Sergeant.paa"};
};
class AL_Corrections5_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Corrections Lieutenant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Corrections5";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\C_Lieutenant.paa"};
};
class AL_Corrections6_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Corrections Captain";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Corrections6";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\C_Captain.paa"};
};
class AL_Trooper1_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] State Trooper Officer";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Trooper1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\T_Officer.paa"};
};
class AL_Trooper2_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] State Trooper Senior Officer";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Trooper2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\T_Senior.paa"};
};
class AL_Trooper3_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] State Trooper Corporal";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Trooper3";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\T_Corporal.paa"};
};
class AL_Trooper4_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] State Trooper Sergeant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Trooper4";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\T_Sergeant.paa"};
};
class AL_Trooper5_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] State Trooper Lieutenant";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Trooper5";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\T_Lieutenant.paa"};
};
class AL_Trooper6_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] State Trooper Captain";
nakedUniform = "U_BasicBody";
uniformClass = "AL_Trooper6";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\T_Captain.paa"};
};
class AL_HWP1_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Highway Patrol Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_HWP1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\AL_HWP_Trooper.paa"};
};
class AL_HWP3_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Highway Patrol Senior Officer Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_HWP2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\CRT\AL_HWP_SnrOfficer.paa"};
};
class AL_HWP4_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Highway Patrol Sergeant Officer Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_HWP2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\CRT\AL_HWP_Sergeant.paa"};
};
class AL_HWP5_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Highway Patrol Lieutenant Officer Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_HWP2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\CRT\AL_HWP_Lieutenant.paa"};
};
class AL_HWP2_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] Highway Patrol Captain Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_HWP2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\CRT\hwpcaptain.paa"};
};
class AL_CRTUniform_Soldier: I_soldier_F
{
_generMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] CRT Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_CRTUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\CRT\CRTUniform.paa"};
side = 1;
};
class AL_SWATUniform_Soldier: I_soldier_F
{
_generMacro = "I_soldier_F";
scope = 2;
displayName = "[PL] SWAT Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_SWATUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\SWATUniform.paa"};
side = 1;
};
class AL_IAUniform_RangeMaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = "[PL] IA Uniform";
nakedUniform = "U_BasicBody";
uniformClass = "AL_IAUniform";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\IAclothing.paa"};
};
class AL_Tactical1_Soldier: B_soldier_F
{
_generMacro = "B_Soldier_F";
scope = 2;
displayName = "[PL] S.W.A.T. Tactical";
uniformAccessories[] = {};
nakedUniform = "U_BasicBody";
uniformClass = "AL_Tactical1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Tactical.paa"};
};
class AL_Tactical2_Soldier: B_soldier_F
{
_generMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] S.W.A.T. Tactical 2";
uniformAccessories[] = {};
nakedUniform = "U_BasicBody";
uniformClass = "AL_Tactical2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Tactical2.paa"};
};
class AL_Tactical3_Soldier: B_soldier_F
{
_generMacro = "B_soldier_F";
scope = 2;
displayName = "[PL] S.W.A.T. Tactical 3";
uniformAccessories[] = {};
nakedUniform = "U_BasicBody";
uniformClass = "AL_Tactical3";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\uniforms\S_Tactical3.paa"};
};
class AL_FTO1_Rangemaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = " [PL] FTO Officer";
uniformAcessories[] = {};
nakedUniform = "u_BasicBody";
uniformClas = "AL_FTO1";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing/data/police/uniforms/fto_ofc.paa"};
};
class AL_FTO2_Rangemaster: B_RangeMaster_F
{
_generMacro = "B_Soldier_F";
scope = 2;
displayName = " [PL] FTO Corporal";
uniformAcessories[] = {};
nakedUniform = "u_BasicBody";
uniformClas = "AL_FTO2";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing/data/police/uniforms/fto_cpl.paa"};
};
class AL_FTO3_Rangemaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = " [PL] FTO Sergeant";
uniformAcessories[] = {};
nakedUniform = "u_BasicBody";
uniformClas = "AL_FTO3";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing/data/police/uniforms/fto_sgt.paa"};
};
class AL_FTO4_Rangemaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = " [PL] FTO Lieutenant";
uniformAcessories[] = {};
nakedUniform = "u_BasicBody";
uniformClas = "AL_FTO4";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing/data/police/uniforms/fto_lt.paa"};
};
class AL_FTO5_Rangemaster: B_RangeMaster_F
{
_generMacro = "B_RangeMaster_F";
scope = 2;
displayName = " [PL] FTO Captain";
uniformAcessories[] = {};
nakedUniform = "u_BasicBody";
uniformClas = "AL_FTO5";
hiddenSelections[] = {"Camo"};
hiddenSelectionsTextures[] = {"AL_Clothing/data/police/uniforms/fto_cpt.paa"};
};
class casper_beautifulbackpack: B_Carryall_base
{
requiredAddons[] = {"A3_Weapons_F_Ammoboxes"};
scope = 2;
displayName = "[PL] Beautiful Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelbeautiful.paa"};
};
class casper_cockbackpack: casper_beautifulbackpack
{
displayName = "[PL] I Love C**k Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelcock.paa"};
};
class casper_facbackpack: casper_beautifulbackpack
{
displayName = "[PL] Fack Me Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelfac.paa"};
};
class casper_hellokittybackpack: casper_beautifulbackpack
{
displayName = "[PL] Hello Kitty Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelhellokitty.paa"};
};
class casper_mcdonaldsbackpack: casper_beautifulbackpack
{
displayName = "[PL] Mc Donalds Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelmcdonalds.paa"};
};
class casper_nemobackpack: casper_beautifulbackpack
{
displayName = "[PL] Nemo Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelnemo.paa"};
};
class casper_ppatrolbackpack: casper_beautifulbackpack
{
displayName = "[PL] Shrek Backpack";
hiddenSelectionsTextures[] = {"AL_Clothing\data\Backpacks\kaelshrek.paa"};
};
class travis_InvisibeCarryall: B_Carryall_oli
{
scope = 2;
displayName = "[PL] Invisible Backpack";
model = "a3\weapons_f\empty.p3d";
author = "Ranger";
class TransportWeapons{};
class TransportMagazines{};
class TransportItems{};
};
class AL_BOIJacketBlack_Soldier: B_Soldier_base_F
{
scope = 1;
model = "\tryk_unit\data\PCUs_G_R.p3d";
hiddenSelections[] = {"camo","camo3"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_JacketJeans.paa","AL_Clothing\data\police\BOI\BOI_Jacket_Black.paa"};
};
class AL_BOIJacketBlue_Soldier: B_Soldier_base_F
{
scope = 1;
model = "\tryk_unit\data\PCUs_G_R.p3d";
hiddenSelections[] = {"camo","camo3"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_JacketJeans.paa","AL_Clothing\data\police\BOI\BOI_Jacket_Blue.paa"};
};
class AL_BOIJacketPurple_Soldier: B_Soldier_base_F
{
scope = 1;
model = "\tryk_unit\data\PCUs_G_R.p3d";
hiddenSelections[] = {"camo","camo3"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_JacketJeans.paa","AL_Clothing\data\police\BOI\BOI_Jacket_Purple.paa"};
};
};
class cfgWeapons
{
class Uniform_Base;
class UniformItem;
class ItemInfo;
class VestItem;
class HeadgearItem;
class V_TacVest_blk_POLICE;
class V_PlateCarrier1_blk;
class V_RebreatherB;
class H_HelmetB_light;
class ItemCore;
class H_Beret_02: ItemCore
{
armor = "0";
passThrough = 0;
};
class H_HelmetB: ItemCore
{
armor = "0";
passThrough = 0;
};
class travis_InmateCounty: Uniform_Base
{
scope = 2;
displayName = "[PL] Inmate Jumpsuit - County";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_InmateCounty_rangemaster";
containerClass = "Supply40";
mass = 0;
};
};
class travis_InmatePrison: Uniform_Base
{
scope = 2;
displayName = "[PL] Inmate Jumpsuit - Prison";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_InmatePrison_rangemaster";
containerClass = "Supply40";
mass = 0;
};
};
class travis_polocookiemonster: Uniform_Base
{
scope = 2;
displayName = "[PL] Cookie Monster Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polocookiemonster_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kiiika_fan: Uniform_Base
{
scope = 2;
displayName = "[PL] Kiiika Fan";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kiiika_fan_rangemaster";
containerClass = "Supply20";
mass = 120;
};
};
class travis_polopsisyndicate: Uniform_Base
{
scope = 2;
displayName = "[PL] PsiSyndicate Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polopsisyndicate_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polosecurity: Uniform_Base
{
scope = 2;
displayName = "[PL] Security Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polosecurity_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_poloapple: Uniform_Base
{
scope = 2;
displayName = "[PL] Apple Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_poloapple_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polohellokitty: Uniform_Base
{
scope = 2;
displayName = "[PL] Hello Kitty Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polohellokitty_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polohellokitty1: Uniform_Base
{
scope = 2;
displayName = "[PL] Hello Kitty 2 Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polohellokitty1_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_poloaustralia: Uniform_Base
{
scope = 2;
displayName = "[PL] Australia Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_poloaustralia_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_spawnclothing: Uniform_Base
{
scope = 2;
displayName = "[PL] Arma-Life Beta Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_spawnclothing_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_wolf: Uniform_Base
{
scope = 2;
displayName = "[PL] Wolf Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_wolf_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_picklerick: Uniform_Base
{
scope = 2;
displayName = "[PL] Pickle-Rick Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_picklerick_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_russianpepe: Uniform_Base
{
scope = 2;
displayName = "[PL] Russian Pepe Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_russianpepe_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_angrypepe: Uniform_Base
{
scope = 2;
displayName = "[PL] Angry Pepe Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_angrypepe_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_trippieredd: Uniform_Base
{
scope = 2;
displayName = "[PL] Trippie Redd Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_trippieredd_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_mexicanfarmer: Uniform_Base
{
scope = 2;
displayName = "[PL] Mexican Farmer Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_mexicanfarmer_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_xxxtenaction: Uniform_Base
{
scope = 2;
displayName = "[PL] XXXtenaction Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_xxxtenaction_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_psi: Uniform_Base
{
scope = 2;
displayName = "[PL] PsiSyn v2 Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_psi_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_psiblack: Uniform_Base
{
scope = 2;
displayName = "[PL] PsiSyn v1 Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_psiblack_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_wizard: Uniform_Base
{
scope = 2;
displayName = "[PL] Wizard Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_wizard_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_vapenation: Uniform_Base
{
scope = 2;
displayName = "[PL] Vape Nation";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_vapenation_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_supreme: Uniform_Base
{
scope = 2;
displayName = "[PL] Supreme Box Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_supreme_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class kojixus_treyway: Uniform_Base
{
scope = 2;
displayName = "[PL] TREYWAY Clothing";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "kojixus_treyway_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Batman: Uniform_Base
{
scope = 2;
displayName = "[PL] Batman Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_batman_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_smokeweed: Uniform_Base
{
scope = 2;
displayName = "[PL] Smoke Weed Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_weed_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polospongebob: Uniform_Base
{
scope = 2;
displayName = "[PL] Spongebob Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polospongebob_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polopatrickstar: Uniform_Base
{
scope = 2;
displayName = "[PL] Patrick Star Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polopatrickstar_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polocanda: Uniform_Base
{
scope = 2;
displayName = "[PL] Canda Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polocanda_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_pololove: Uniform_Base
{
scope = 2;
displayName = "[PL] I Love Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_pololove_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polofuck: Uniform_Base
{
scope = 2;
displayName = "[PL] Fuck Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polofuck_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polococacola: Uniform_Base
{
scope = 2;
displayName = "[PL] Coca Cola Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polococacola_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_poloangelo: Uniform_Base
{
scope = 2;
displayName = "[PL] Michel Angelo Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_poloangelo_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polomrbong: Uniform_Base
{
scope = 2;
displayName = "[PL] Mr Bong Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polomrbong_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polomoon: Uniform_Base
{
scope = 2;
displayName = "[PL] Pink Floyd Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polomoon_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polodino: Uniform_Base
{
scope = 2;
displayName = "[PL] Dinosaur Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polodino_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polofucka3pl: Uniform_Base
{
scope = 2;
displayName = "[PL] Fuck A3PL Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polofucka3pl_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_polostunt: Uniform_Base
{
scope = 2;
displayName = "[PL] Stunt Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_polostunt_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_poloface: Uniform_Base
{
scope = 2;
displayName = "[PL] Face Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_poloface_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_avengers: Uniform_Base
{
scope = 2;
displayName = "[PL] Avengers Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_avengers_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_doge: Uniform_Base
{
scope = 2;
displayName = "[PL] Doge Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_doge_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_fuckit: Uniform_Base
{
scope = 2;
displayName = "[PL] Fuck It Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_fuckit_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_illuminati: Uniform_Base
{
scope = 2;
displayName = "[PL] Illuminati Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_illuminati_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_jeff: Uniform_Base
{
scope = 2;
displayName = "[PL] Jeff Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_jeff_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_kappa: Uniform_Base
{
scope = 2;
displayName = "[PL] Kappa Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_kappa_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_married: Uniform_Base
{
scope = 2;
displayName = "[PL] Married Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_married_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_marvel: Uniform_Base
{
scope = 2;
displayName = "[PL] Marvel Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_marvel_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_mexicantroll: Uniform_Base
{
scope = 2;
displayName = "[PL] Mexican Troll Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_mexicantroll_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_minion: Uniform_Base
{
scope = 2;
displayName = "[PL] Minion Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_minion_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_monstercat: Uniform_Base
{
scope = 2;
displayName = "[PL] Monstercat Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_monstercat_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_nike: Uniform_Base
{
scope = 2;
displayName = "[PL] Nike Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_nike_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_pancake: Uniform_Base
{
scope = 2;
displayName = "[PL] Pancake Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_pancake_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_shield: Uniform_Base
{
scope = 2;
displayName = "[PL] S.H.I.E.L.D. Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_shield_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_smile: Uniform_Base
{
scope = 2;
displayName = "[PL] Smile Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_smile_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_timmac: Uniform_Base
{
scope = 2;
displayName = "[PL] Timmac Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_timmac_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_tux: Uniform_Base
{
scope = 2;
displayName = "[PL] Tux Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_tux_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_twitch: Uniform_Base
{
scope = 2;
displayName = "[PL] Twitch Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_twitch_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_yaoming: Uniform_Base
{
scope = 2;
displayName = "[PL] Yao Ming Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_yaoming_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Butts: Uniform_Base
{
scope = 2;
displayName = "[PL] Kiika Butts Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_Butts_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Snoopy: Uniform_Base
{
scope = 2;
displayName = "[PL] Snoopy Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_Snoopy_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Coco: Uniform_Base
{
scope = 2;
displayName = "[PL] Coco Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_Coco_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Ciggy: Uniform_Base
{
scope = 2;
displayName = "[PL] Ciggy Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_Ciggy_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Adidas: Uniform_Base
{
scope = 2;
displayName = "[PL] Adidas Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_Adidas_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_ReclaimJoey: Uniform_Base
{
scope = 2;
displayName = "[PL] ReclaimJoey Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_ReclaimJoey_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_Feature: Uniform_Base
{
scope = 2;
displayName = "[PL] FeatureTV Polo";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_Feature_rangemaster";
containerClass = "Supply20";
mass = 80;
};
};
class travis_emstraineeuniform: Uniform_Base
{
scope = 2;
displayName = "[PL] EMS Trainee Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_F\BLUFOR\b_soldier_01.p3d";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_emstraineeuniform_soldier";
containerClass = "Supply50";
mass = 80;
};
};
class travis_emsemtuniform: Uniform_Base
{
scope = 2;
displayName = "[PL] EMS EMT Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_F\BLUFOR\b_soldier_01.p3d";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_emsemtuniform_soldier";
containerClass = "Supply50";
mass = 80;
};
};
class fireuniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Fire and Rescue Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_F\BLUFOR\b_soldier_01.p3d";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "fireuniform_soldier";
containerClass = "Supply50";
mass = 80;
};
};
class travis_emsparamedicuniform: Uniform_Base
{
scope = 2;
displayName = "[PL] EMS Paramedic Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_F\BLUFOR\b_soldier_01.p3d";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_emsparamedicuniform_soldier";
containerClass = "Supply50";
mass = 80;
};
};
class travis_emsseniorparamedicuniform: Uniform_Base
{
scope = 2;
displayName = "[PL] EMS Senior Paramedic Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_F\BLUFOR\b_soldier_01.p3d";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_emsseniorparamedicuniform_soldier";
containerClass = "Supply50";
mass = 80;
};
};
class travis_emscommanduniform: Uniform_Base
{
scope = 2;
displayName = "[PL] EMS Command Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_F\BLUFOR\b_soldier_01.p3d";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "travis_emscommanduniform_soldier";
containerClass = "Supply50";
mass = 80;
};
};
class travis_TraineeVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS Trainee Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Probie.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class travis_RideAlongVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS Ride Along Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Ride_Along_Vest.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class travis_ParamedicVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS Paramedic Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Paramedic.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class FirstResponder_vest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS First Responders Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\ems_vest.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class travis_EMTVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS EMT Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\EMT.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class travis_CommandVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS Command Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Command_Vest.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class travis_SARVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] EMS Search and Rescue Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\medic\Search_and_Rescue.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 40;
armor = 80;
passThrough = 0.5;
hiddenSelections[] = {"camo"};
};
};
class AL_DOJVest: V_TacVest_blk_POLICE
{
scope = 2;
displayName = "[PL] DOJ Vest";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_tacticalvest";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_DOJVest_Standard.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 30;
passThrough = 0.1;
hiddenSelections[] = {"camo"};
class HitpointsProtectionInfo
{
class Chest
{
HitpointName = "HitChest";
armor = 10;
PassThrough = 0.1;
};
class Diaphragm
{
HitpointName = "HitDiaphragm";
armor = 10;
PassThrough = 0.1;
};
class Abdomen
{
hitpointName = "HitAbdomen";
armor = 10;
passThrough = 0.1;
};
class Body
{
hitpointName = "HitBody";
passThrough = 0.1;
};
};
};
};
class AL_DOJVestCommand: AL_DOJVest
{
displayName = "[PL] DOJ Vest Command";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_DOJVest_Command.paa"};
class ItemInfo: VestItem
{
uniformModel = "A3\Characters_F\common\equip_tacticalvest";
containerClass = "Supply300";
mass = 30;
};
};
class AL_SWATHeavyVest: V_PlateCarrier1_blk
{
scope = 2;
displayName = "[PL] S.W.A.T. Plate Carrier";
model = "A3\characters_f\BLUFOR\equip_b_vest02";
picture = "\AL_Clothing\data\ui\icon_polo";
hiddenSelections[] = {"camo"};
descriptionShort = "Global Heavy Armor";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_SWATHeavy.paa"};
class ItemInfo: VestItem
{
uniformModel = "\A3\characters_f\BLUFOR\equip_b_vest02";
containerClass = "Supply350";
mass = 30;
passThrough = 0.1;
hiddenSelections[] = {"camo"};
class HitpointsProtectionInfo
{
class Chest
{
HitpointName = "HitChest";
armor = 17;
PassThrough = 0.1;
};
class Diaphragm
{
HitpointName = "HitDiaphragm";
armor = 17;
PassThrough = 0.1;
};
class Abdomen
{
hitpointName = "HitAbdomen";
armor = 17;
passThrough = 0.1;
};
class Body
{
hitpointName = "HitBody";
passThrough = 0.1;
};
};
};
};
class AL_InternalAffairsHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Internal Affairs Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_InternalAffairs.paa"};
};
class AL_CRTHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] CRT Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CRTHeavy.paa"};
};
class AL_PoliceHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Police Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_PoliceHeavy.paa"};
};
class AL_TrooperHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Trooper Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_TrooperHeavy.paa"};
};
class AL_CorrectionsHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Corrections Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CorrectionsHeavy.paa"};
};
class AL_CIDHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] C.I.D Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CIDHeavy.paa"};
};
class AL_CIDHeavyGreenVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Green C.I.D Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CIDHeavyGreen.paa"};
};
class AL_CERTHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] C.E.R.T. Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CERTHeavy.paa"};
};
class AL_HWPHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Highway Patrol Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_HWPHeavy.paa"};
};
class AL_CartelHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Cartel Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_CartelHeavy.paa"};
};
class AL_MafiaHeavyVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Mafia Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_MafiaHeavy.paa"};
};
class AL_CivHeavyVest1: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Plate Carrier 1";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_CivHeavy1.paa"};
};
class AL_CivHeavyVest2: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Plate Carrier 2";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_CivHeavy2.paa"};
};
class AL_CivHeavyVest3: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Plate Carrier 3";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_CivHeavy3.paa"};
};
class AL_CivHeavyVest4: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Plate Carrier 4";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_CivHeavy4.paa"};
};
class AL_CivHeavyVest5: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Plate Carrier 5";
hiddenSelectionsTextures[] = {"AL_Clothing\data\faction\vests\AL_CivHeavy5.paa"};
};
class AL_TacticALHelmet: H_HelmetB_light
{
scope = 2;
weaponPoolAvailable = 1;
displayName = "[PL] S.W.A.T. Helmet";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\a3\characters_f\BLUFOR\headgear_b_helmet_light";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\al_helmet.paa"};
class ItemInfo: HeadgearItem
{
mass = 20;
uniformModel = "\a3\characters_f\BLUFOR\headgear_b_helmet_light";
modelSides[] = {3,1};
armor = 10;
passThrough = 0.1;
hiddenSelections[] = {"camo"};
};
};
class AL_LosDCartelVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Los Diablos Cartel Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\losdvest.paa"};
};
class AL_BandidosVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Bandidos Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\bandidosvest.paa"};
};

class AL_SosaVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Sosa's Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\sosavest.paa"};
};
class AL_SonoraVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Sonora Cartel Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\sonoravest.paa"};
};
class AL_ReddingtonVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Reddington Family Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\reddingtonvest.paa"};
};
class AL_PeakyBlindersVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Peaky Blinders Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\peakyblindersvest.paa"};
};
class AL_BrotherhoodUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Brotherhood Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_brotherhooduniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_BrotherhoodVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Brotherhood Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\brotherhoodvest.paa"};
};
class AL_MillersUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Millers Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_millersuniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_MillersVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Millers Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\millersvest.paa"};
};
class AL_ChauUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Chau Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_chauuniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_ChauVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Chau Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\CHAUVEST3.paa"};
};
class AL_SyndicateUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] The Syndicate Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_syndicateuniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SyndicateVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] The Syndicate Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\syndicatevest.paa"};
};
class AL_atlasUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Atlas Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_atlasuniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_atlasVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Atlast Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\atlasvest.paa"};
};
class AL_reprisalUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Reprisal Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_reprisaluniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_ReprisalVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Reprisal Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\reprisalvest.paa"};
};
class AL_bratvaUniform: Uniform_Base
    {
        scope = 2;
        displayName = "[PL] Bratva Gang Uniform";
        author="Michael Reddington";
        picture = "\AL_Clothing\data\ui\icon_polo";
        model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";

        class ItemInfo: UniformItem
        {
            uniformModel = "-";
            uniformClass = "al_bratvauniform_soldier";
            containerClass = "Supply20";
            mass = 80;
        };
    };
class AL_BratvaVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Bratva Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\bratvavest.paa"};
};
class AL_devilshenchmenUniform: Uniform_Base
    {
        scope = 2;
        displayName = "[PL] Devils Henchmen Gang Uniform";
        author="Michael Reddington";
        picture = "\AL_Clothing\data\ui\icon_polo";
        model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";

        class ItemInfo: UniformItem
        {
            uniformModel = "-";
            uniformClass = "al_devilshenchmenuniform_soldier";
            containerClass = "Supply20";
            mass = 80;
        };
    };
class AL_devilshenchmenVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Devils Henchmen Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\devilshenchmenvest.paa"};
};
class AL_VigilantesVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Vigilantes Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\vigilantesvest.paa"};
};
class AL_OutlawVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] The Underground Outlaw Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\outlawsvest.paa"};
};
class AL_ffUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Filthy Fraggers Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_ffuniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_ffVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Filthy Fraggers Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\ffvest.paa"};
};

class AL_YakuzaUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Yakuza Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_yakuzauniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_YakuzaVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Yakuza Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\yakuzavest.paa"};
};

class AL_ManarelloUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Manarello Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "al_manarellouniform_soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_ManarelloVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Manarello Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\manarellovest.paa"};
};

class AL_UrFatUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] Ur Fat Gang Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
author = "Arma-Life";
hiddenSelections[] = {"camo","camo2"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\croftuni.paa","AL_Clothing\data\gangs\croftuniblank.paa"};
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_UrFatUniform_Soldier";
containerClass = "Supply40";
uniformType = "";
mass = 60;
};
};
class AL_UrFatVest: AL_SWATHeavyVest
{
scope = 2;
displayName = "[PL] Ur Fat Plate Carrier";
hiddenSelectionsTextures[] = {"AL_Clothing\data\gangs\croftvest.paa"};
};
class AL_PoliceRebreatherHeavyVest: V_RebreatherB
{
scope = 2;
displayName = "[PL] Police Rebreather";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\equip_rebreather.p3d";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"\AL_Clothing\data\police\uniforms\prebreather.paa"};
descriptionShort = "Global Heavy Armor";
class ItemInfo: VestItem
{
uniformModel = "\A3\Characters_F\Common\equip_rebreather";
containerClass = "Supply0";
mass = 30;
passThrough = 0.1;
hiddenSelections[] = {"camo"};
vestType = "Rebreather";
hiddenUnderwaterSelections[] = {"hide"};
shownUnderwaterSelections[] = {"unhide","unhide2"};
hiddenUnderwaterSelectionsTextures[] = {"\A3\characters_f\common\data\diver_equip_nato_co.paa","\A3\characters_f\common\data\diver_equip_nato_co.paa","\A3\characters_f\data\visors_ca.paa"};
class HitpointsProtectionInfo
{
class Chest
{
HitpointName = "HitChest";
armor = 17;
PassThrough = 0.1;
};
class Diaphragm
{
HitpointName = "HitDiaphragm";
armor = 17;
PassThrough = 0.1;
};
class Abdomen
{
hitpointName = "HitAbdomen";
armor = 17;
passThrough = 0.1;
};
class Body
{
hitpointName = "HitBody";
passThrough = 0.1;
};
};
};
};
class AL_PoliceWetsuit: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Wetsuit";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\characters_f\Common\diver_slotable.p3d";
class ItemInfo: UniformItem
{
uniformModel = "\A3\characters_f\Common\diver_slotable.p3d";
uniformClass = "AL_PoliceWetsuit_Diver";
containerClass = "Supply140";
uniformType = "Neopren";
mass = 60;
};
};
class AL_Police0: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Cadet";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police0_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police1: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police1_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police2: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Senior Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police2_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police3: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Corporal";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police3_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police4: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Sergeant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police4_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police5: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Lieutenant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police5_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police6: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Captain";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police6_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police16: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Inspector";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police16_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police17: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Superintendent";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police17_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police18: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Assistant Chef";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police18_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police19: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Chef";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police19_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Police20: Uniform_Base
{
scope = 2;
displayName = "[PL] Police Commissioner";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Police20_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_FTO1: Uniform_Base
{
scope = 2;
displayName = "[PL] FTO Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_FTO1_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_FTO2: Uniform_Base
{
scope = 2;
displayName = "[PL] FTO Corporal";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_FTO2_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_FTO3: Uniform_Base
{
scope = 2;
displayName = "[PL] FTO Sergeant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_FTO3_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_FTO4: Uniform_Base
{
scope = 2;
displayName = "[PL] FTO Lieutenant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_FTO4_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_FTO5: Uniform_Base
{
scope = 2;
displayName = "[PL] FTO Captain";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_FTO5_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SWAT2: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Patrol Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_SWAT2_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SWAT3: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Corporal";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_SWAT3_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SWAT4: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Sergeant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_SWAT4_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SWAT5: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Lieutenant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_SWAT5_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SWAT6: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Captain";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_SWAT6_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Corrections1: Uniform_Base
{
scope = 2;
displayName = "[PL] Corrections Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Corrections1_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Corrections2: Uniform_Base
{
scope = 2;
displayName = "[PL] Corrections Senior Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Corrections2_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Corrections3: Uniform_Base
{
scope = 2;
displayName = "[PL] Corrections Corporal";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Corrections3_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Corrections4: Uniform_Base
{
scope = 2;
displayName = "[PL] Corrections Sergeant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Corrections4_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Corrections5: Uniform_Base
{
scope = 2;
displayName = "[PL] Corrections Lieutenant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Corrections5_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Corrections6: Uniform_Base
{
scope = 2;
displayName = "[PL] Corrections Captain";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Corrections6_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Trooper1: Uniform_Base
{
scope = 2;
displayName = "[PL] State Trooper Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Trooper1_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Trooper2: Uniform_Base
{
scope = 2;
displayName = "[PL] State Trooper Officer";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Trooper2_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Trooper3: Uniform_Base
{
scope = 2;
displayName = "[PL] State Trooper Corporal";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Trooper3_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Trooper4: Uniform_Base
{
scope = 2;
displayName = "[PL] State Trooper Sergeant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Trooper4_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Trooper5: Uniform_Base
{
scope = 2;
displayName = "[PL] State Trooper Lieutenant";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Trooper5_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Trooper6: Uniform_Base
{
scope = 2;
displayName = "[PL] State Trooper Captain";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Trooper6_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_HWP1: Uniform_Base
{
scope = 2;
displayName = "[PL] Highway Patrol Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_HWP1_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_HWP3: Uniform_Base
{
scope = 2;
displayName = "[PL] Highway Patrol Senior Officer Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_HWP3_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_HWP4: Uniform_Base
{
scope = 2;
displayName = "[PL] Highway Patrol Lieutenant Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_HWP4_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_HWP2: Uniform_Base
{
scope = 2;
displayName = "[PL] Highway Patrol Captain Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_HWP2_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_CRTUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] CRT Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_CRTUniform_Soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_SWATUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] SWAT Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_SWATUniform_Soldier";
containerClass = "Supply20";
mass = 80;
};
};
class AL_IAUniform: Uniform_Base
{
scope = 2;
displayName = "[PL] IA Uniform";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_IAUniform_RangeMaster";
containerClass = "Supply20";
mass = 80;
};
};
class AL_Tactical1: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Tactical";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Tactical1_Soldier";
containerClass = "Supply40";
mass = 80;
};
};
class AL_Tactical2: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Tactical 2";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Tactical2_Soldier";
containerClass = "Supply40";
mass = 80;
};
};
class AL_Tactical3: Uniform_Base
{
scope = 2;
displayName = "[PL] S.W.A.T. Tactical 3";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_Tactical3_Soldier";
containerClass = "Supply40";
mass = 80;
};
};
class AL_BOIJacketBlack: Uniform_Base
{
scope = 2;
displayName = "[PL] BOI Jacket Black";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_BOIJacketBlack_Soldier";
containerClass = "Supply40";
mass = 40;
};
};
class AL_BOIJacketBlue: Uniform_Base
{
scope = 2;
displayName = "[PL] BOI Jacket Blue";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_BOIJacketBlue_Soldier";
containerClass = "Supply40";
mass = 40;
};
};
class AL_BOIJacketPurple: Uniform_Base
{
scope = 2;
displayName = "[PL] BOI Jacket Purple";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
class ItemInfo: UniformItem
{
uniformModel = "-";
uniformClass = "AL_BOIJacketPurple_Soldier";
containerClass = "Supply40";
mass = 40;
};
};
class AL_BOIBeret: H_Beret_02
{
scope = 2;
displayName = "[PL] BOI Beret";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "\a3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
author = "Arma-Life";
descriptionShort = "";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_Beret.paa"};
};
class AL_BOI_Beanie_Black: H_HelmetB
{
scope = 2;
displayName = "[PL] BOI Beanie Black";
picture = "\AL_Clothing\data\ui\icon_polo";
model = "tryk_unit\data\woolhat.p3d";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_Beanie_Black.paa"};
class ItemInfo: HeadgearItem
{
mass = 5;
allowedSlots[] = {901,605};
uniformModel = "tryk_unit\data\woolhat.p3d";
hiddenSelections[] = {"camo"};
modelSides[] = {1,2,3};
armor = 0.5;
passThrough = 0.5;
explosionShielding = 0.2;
};
};
class AL_BOI_Beanie_Blue: AL_BOI_Beanie_Black
{
scope = 2;
displayName = "[PL] BOI Beanie Blue";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_Beanie_Blue.paa"};
};
class AL_BOI_Beanie_Purple: AL_BOI_Beanie_Black
{
scope = 2;
displayName = "[PL] BOI Beanie Purple";
hiddenSelections[] = {"camo"};
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\BOI\BOI_Beanie_Purple.paa"};
};
};