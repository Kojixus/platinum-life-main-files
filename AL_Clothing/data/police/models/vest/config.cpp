class DefaultEventhandlers;
class CfgPatches
{
class AL_PoliceVest_Patches
{
units[] = {};
weapons[] = {"AL_PoliceVest","AL_CorrectionsVest","AL_TrooperVest","AL_SWATVest","AL_CIDVest","AL_CERTVest"};
requiredVersion = 0.1;
requiredAddons[] = {"A3_Characters_F","A3_Data_F","A3_Characters_F_Common","A3_Characters_F_Proxies"};
};
};
class UniformSlotInfo;
class cfgWeapons
{
class InventoryItem_Base_F;
class ItemCore;
class VestItem: InventoryItem_Base_F
{
type = 701;
hiddenSelections[] = {};
armor = "5*0";
passThrough = 1;
hitpointName = "HitBody";
};
class Vest_Camo_Base: ItemCore
{
scope = 0;
allowedSlots[] = {901};
hiddenSelections[] = {"camo"};
class ItemInfo: VestItem
{
hiddenSelections[] = {"camo"};
maximumLoad = 0;
mass = 0;
};
};
class AL_PoliceVest: Vest_Camo_Base
{
scope = 2;
displayName = "[AL] Police Vest";
model = "AL_Clothing\data\police\models\vest\AL_Vest.p3d";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_Police.paa"};
class ItemInfo: VestItem
{
uniformModel = "AL_Clothing\data\police\models\vest\AL_Vest.p3d";
containerClass = "Supply300";
mass = 60;
passThrough = 0.13;
hiddenSelections[] = {"camo"};
class HitpointsProtectionInfo
{
class Chest
{
HitpointName = "HitChest";
armor = 40;
PassThrough = 0.13;
};
class Diaphragm
{
HitpointName = "HitDiaphragm";
armor = 40;
PassThrough = 0.13;
};
class Abdomen
{
hitpointName = "HitAbdomen";
armor = 40;
passThrough = 0.13;
};
class Body
{
hitpointName = "HitBody";
passThrough = 0.13;
};
};
};
};
class AL_CorrectionsVest: AL_PoliceVest
{
scope = 2;
displayName = "[AL] Corrections Vest";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_Corrections.paa"};
};
class AL_TrooperVest: AL_PoliceVest
{
scope = 2;
displayName = "[AL] Trooper Vest";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_Trooper.paa"};
};
class AL_SWATVest: AL_PoliceVest
{
scope = 2;
displayName = "[AL] S.W.A.T. Vest";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_SWAT.paa"};
};
class AL_CIDVest: AL_PoliceVest
{
scope = 2;
displayName = "[AL] C.I.D. Vest";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CID.paa"};
};
class AL_CERTVest: AL_PoliceVest
{
scope = 2;
displayName = "[AL] C.E.R.T. Vest";
hiddenSelectionsTextures[] = {"AL_Clothing\data\police\vests\AL_CERT.paa"};
};
};