class DefaultEventhandlers;
class UniformSlotInfo;
class CfgPatches
{
class AL_Headgear_PatrolHat
{
units[] = {};
weapons[] = {"AL_PoliceHat"};
requiredVersion = 0.1;
requiredAddons[] = {"A3_Characters_F","AL_Clothing"};
};
};
class cfgWeapons
{
class InventoryItem_Base_F;
class ItemCore;
class HeadgearItem: InventoryItem_Base_F
{
allowedSlots[] = {901,605};
type = 605;
hiddenSelections[] = {};
hitpointName = "HitHead";
};
class AL_PoliceHat: ItemCore
{
scope = 2;
displayName = "[AL] Police Patrol Hat";
picture = "\A3\characters_f\Data\UI\icon_H_Cap_blk_CA.paa";
model = "AL_Clothing\data\police\models\hat\AL_PatrolHat";
class ItemInfo: HeadgearItem
{
mass = 10;
uniformModel = "AL_Clothing\data\police\models\hat\AL_PatrolHat";
allowedSlots[] = {801,901,701,605};
modelSides[] = {6};
armor = 0;
passThrough = 1;
hiddenSelections[] = {"camo"};
};
};
};