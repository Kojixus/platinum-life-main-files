class DefaultEventhandlers;
class CfgPatches
{
class AL_DutyBelt
{
units[] = {"AL_PoliceBelt"};
weapons[] = {};
requiredVersion = 0.1;
requiredAddons[] = {"A3_Characters_F","A3_Data_F"};
};
};
class UniformSlotInfo;
class CfgVehicles
{
class B_Carryall_Base;
class I_soldier_F;
class AL_PoliceBelt: B_Carryall_Base
{
scope = 2;
displayName = "[AL] Police Duty Belt";
model = "AL_Clothing\data\police\models\belt\AL_PoliceBelt";
author = "Arma-Life Development Team";
maximumLoad = 300;
mass = 80;
scopeCurator = 2;
};
};