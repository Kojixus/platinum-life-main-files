class CfgPatches
{
	class ArmalifeCore
	{
		units[]={};
		weapons[]={};
		requiredVersion = 0.036;
		requiredAddons[]={};
	};
};

class CfgMusic {
     tracks[] = {};
     class cam_intro {
        name = "cam_intro";
        sound[] = {"\ArmalifeCore\sounds\intro_song.ogg", 1.0, 1};
    };
};

class CfgSounds {
    sounds[] = {};

    class bankAlarm {
        name = "bankAlarm";
        sound[] = {"\ArmalifeCore\sounds\bankAlarm.ogg", 1, 1, 200};
        titles[] = {};
    };

    class burglaryAlarm {
        name = "burglaryAlarm";
        sound[] = {"\ArmalifeCore\sounds\burglaryAlarm.ogg", 0.8, 1, 200};
        titles[] = {};
    };

    class TestCorrect {
        name = "TestCorrect";
        sound[] = {"\ArmalifeCore\sounds\TestCorrect.ogg", 1, 1};
        titles[] = {};
    };

    class lootbox {
        name = "lootbox";
        sound[] = {"\ArmalifeCore\sounds\lootbox.ogg", 1, 1};
        titles[] = {};
    };

    class rockhit {
        name = "rockhit";
        sound[] = {"\ArmalifeCore\sounds\rockhit.ogg", 1, 1};
        titles[] = {};
    };

    class notificationRecieved {
        name = "notificationRecieved";
        sound[] = {"\ArmalifeCore\sounds\notificationRecieved.ogg", 1, 1};
        titles[] = {};
    };

    class openphone {
        name = "openphone";
        sound[] = {"\ArmalifeCore\sounds\openphone.ogg", 1, 1, 30};
        titles[] = {};
    };

    class TestComplete {
        name = "TestComplete";
        sound[] = {"\ArmalifeCore\sounds\TestComplete.ogg", 1, 1};
        titles[] = {};
    };

    class TestFailure {
        name = "TestFailure";
        sound[] = {"\ArmalifeCore\sounds\TestFailure.ogg", 1, 1};
        titles[] = {};
    };

    class satelliteOff {
        name = "satelliteOff";
        sound[] = {"\ArmalifeCore\sounds\satelliteOff.ogg", 1.3, 1};
        titles[] = {};
    };

    class chargeSound {
      name = "chargeSound";
      sound[] = {"\ArmalifeCore\sounds\chargeSound.ogg", 1, 1, 100};
      titles[] = {};
    };

    class zipties {
      name = "zipties";
      sound[] = {"\ArmalifeCore\sounds\ziptie.ogg", 1, 1};
      titles[] = {};
    };

    class panic {
      name = "panic";
      sound[] = {"\ArmalifeCore\sounds\panic.ogg", 0.9, 1};
      titles[] = {};
    };

    class panic2 {
      name = "panic2";
      sound[] = {"\ArmalifeCore\sounds\panic2.ogg", 1.1, 1};
      titles[] = {};
    };

    class buy {
      name = "buy";
      sound[] = {"\ArmalifeCore\sounds\buy.ogg", 1, 1};
      titles[] = {};
    };

    class weed {
      name = "weed";
      sound[] = {"\ArmalifeCore\sounds\weed.ogg", 1, 1, 50};
      titles[] = {};
    };

    class pdrill {
      name = "pdrill";
      sound[] = {"\ArmalifeCore\sounds\pdrill.ogg", 1, 1};
      titles[] = {};
    };

	class jailbreak
	{
		name = "jailbreak";
		sound[] = {"\ArmalifeCore\sounds\jailbreak.ogg", 1.0, 1, 1000};
		titles[] = {};
	};

    class CarAlarm {
        name = "CarAlarm";
        sound[] = {"\ArmalifeCore\sounds\caralarm.ogg", 1.0, 1};
        titles[] = {};
    };

    class repair {
        name = "repair";
        sound[] = {"\ArmalifeCore\sounds\repair.ogg", 1.0, 1, 50};
        titles[] = {};
    };

    class atmButton
    {
        name = "atmButton";
        sound[] = {"\ArmalifeCore\sounds\atmButton.ogg", 1.0, 1};
        titles[] = {};
    };

    class medicSiren {
        name = "medicSiren";
        sound[] = {"\ArmalifeCore\sounds\medicSiren.ogg", 1.0, 1};
        titles[] = {};
    };

    class rumbler {
        name = "rumbler";
        sound[] = {"\ArmalifeCore\sounds\rumbler.ogg", 1.0, 1};
        titles[] = {};
    };

    class flashbang {
        name = "flashbang";
        sound[] = {"\ArmalifeCore\sounds\flashbang.ogg", 1.0, 1};
        titles[] = {};
    };

    class mining {
        name = "mining";
        sound[] = {"\ArmalifeCore\sounds\mining.ogg", 1.0, 1};
        titles[] = {};
    };

    class harvest {
        name = "harvest";
        sound[] = {"\ArmalifeCore\sounds\harvest.ogg", 1.0, 1};
        titles[] = {};
    };

    class carlockunlock {
        name = "carlockunlock";
        sound[] = {"\ArmalifeCore\sounds\carlockunlock.ogg", 1.5, 1};
        titles[] = {};
    };
};
