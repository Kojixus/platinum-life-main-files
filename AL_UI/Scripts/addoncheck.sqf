_patches = configFile >> 'CfgPatches';
_ret = false;
for '_i' from 0 to (count _patches)-1 do {
	_patch = _patches select _i;
	if (isClass _patch) then {
		if ((configName _patch) isEqualTo "FHQ_Weapons") then {
			_ret = true;
		};
	};
};

if (!_ret) then {
	createDialog "AL_ModsMissing";
};