class CfgPatches
{
	class AL_UI
	{
		units[]={};
		weapons[]={};
		requiredAddons[]=
		{
			"A3_Map_Malden",
			"A3_Data_F",
			"A3_Ui_F",
			"A3_Functions_F",
			"A3_UiFonts_F",
			"A3_Map_Stratis",
			"A3_Map_Altis",
			"A3_Map_VR",
			"A3_Map_Stratis_Scenes",
			"A3_Map_VR_Scenes",
			"A3_Map_Altis_Scenes",
			"A3_Ui_F_Data"
		};
		fileName="AL_UI.pbo";
		requiredVersion=0.01;
		author[]={"Travis Butts"};
	};
};

class CfgFunctions
{
	class UI
	{
		tag="AL";
		class init
		{
			file="\AL_UI\Scripts\UI";
			class initDisplay {};
		};
	};
};

class CfgSounds
{
	tracks[]={};

 	class wind
 	{
		name = "wind";	
		sound[] = {"\AL_UI\wind.ogg",0.6,1};
		titles[] = {};
 	};
};


class CfgMissions
{
	class Cutscenes
	{
		class AL
		{
			directory="AL_UI\missions\AL.POPLife_MetropolisV2";
		};
	};
};


class CfgWorlds
{
	class CAWorld;
	class Altis: CAWorld
	{
		cutscenes[]=
		{
			"AL"
		};
	};
	class Malden: CAWorld
	{
		cutscenes[]=
		{
			"AL"
		};
	};
	class Stratis: CAWorld
	{
		cutscenes[]=
		{
			"AL"
		};
	};
	class VR: CAWorld
	{
		cutscenes[]=
		{
			"AL"
		};
	};
	class POPLife_MetropolisV2: CAWorld
	{
		cutscenes[]=
		{
			"AL"
		};
	};
	initWorld="POPLife_MetropolisV2";
	demoWorld="POPLife_MetropolisV2";
};

class CfgDiary
{
	class FixedPages
	{
		class Units
		{
			type = "";
		};
		class Players
		{
			type = "";
		};
		class Statistics
		{
			type = "";
		};
	};
};

class RscListNBox;
class RscPicture;
class RscText;
class IGUIBack;
class RscActiveText;
class RscCombo;
class RscListBox;
class RscProgress;
class RscPictureKeepAspect;
class RscDisplayInventory_DLCTemplate;
class RscStructuredText;
class RscTitle;
class RscButtonMenuOK;
class RscControlsGroupNoHScrollbars;
class RscHTML;
class RscStandardDisplay;
class RscVignette;
class RscControlsGroupNoScrollbars;
class RscFrame;
class CA_Title;
class RscDebugConsole;
class RscTrafficLight;
class RscFeedback;
class RscMessageBox;
class ShortcutPos;
class RscButtonMenuCancel;
class RscButtonMenuSteam;
class RscActivePicture;
class RscButtonMenuMain;
class RscControlsGroup;
class RscEdit;
class RscActivePictureKeepAspect;
class RscMapControl;
class RscShortControl;
class RscCheckBox;
class RscShortcutButton;
class ScrollBar;
class RscXListBox;
class RscButtonTextOnly;

class RscButton
{
	idc = -1;
	style = 2;
	x = 0;
	y = 0;
	w = 0.095589;
	h = 0.039216;
	shadow = 2;
	font = "RobotoCondensed";
	sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	url = "";
	colorText[] = {1,1,1,1.0};
	colorDisabled[] = {1,1,1,0.25};
	colorBackground[] = {0,0,0,0.5};
	colorBackgroundActive[] = {0,0,0,1};
	colorBackgroundDisabled[] = {0,0,0,0.5};
	colorFocused[] = {0,0,0,1};
	colorShadow[] = {0,0,0,0};
	offsetX = 0;
	offsetY = 0;
	offsetPressedX = 0;
	offsetPressedY = 0;
	colorBorder[] = {0,0,0,1};
	borderSize = 0.0;
	soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
	soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
	soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};

class RscButtonMenu: RscShortcutButton
{
	idc = -1;
	type = 16;
	style = "0x02 + 0xC0";
	default = 0;
	shadow = 0;
	x = 0;
	y = 0;
	w = 0.095589;
	h = 0.039216;
	animTextureNormal = "#(argb,8,8,3)color(1,1,1,1)";
	animTextureDisabled = "#(argb,8,8,3)color(1,1,1,1)";
	animTextureOver = "#(argb,8,8,3)color(1,1,1,1)";
	animTextureFocused = "#(argb,8,8,3)color(1,1,1,1)";
	animTexturePressed = "#(argb,8,8,3)color(1,1,1,1)";
	animTextureDefault = "#(argb,8,8,3)color(1,1,1,1)";
	colorBackground[] = {0,0,0,0.8};
	colorBackgroundFocused[] = {1,1,1,1};
	colorBackground2[] = {0.75,0.75,0.75,1};
	color[] = {1,1,1,1};
	colorFocused[] = {0,0,0,1};
	color2[] = {0,0,0,1};
	colorText[] = {1,1,1,1};
	colorDisabled[] = {1,1,1,0.25};
	textSecondary = "";
	colorSecondary[] = {1,1,1,1};
	colorFocusedSecondary[] = {0,0,0,1};
	color2Secondary[] = {0,0,0,1};
	colorDisabledSecondary[] = {1,1,1,0.25};
	sizeExSecondary = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	fontSecondary = "PuristaLight";
	period = 1.2;
	periodFocus = 1.2;
	periodOver = 1.2;
	size = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	tooltipColorText[] = {1,1,1,1};
	tooltipColorBox[] = {1,1,1,1};
	tooltipColorShade[] = {0,0,0,0.65};
	class TextPos
	{
		left = "0.25 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
		top = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) - 		(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)) / 2";
		right = 0.005;
		bottom = 0.0;
	};
	class Attributes
	{
		font = "PuristaLight";
		color = "#E5E5E5";
		align = "left";
		shadow = "false";
	};
	class ShortcutPos
	{
		left = "5.25 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
		top = 0;
		w = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
		h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	};
	soundEnter[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundEnter",0.09,1};
	soundPush[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundPush",0.09,1};
	soundClick[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundClick",0.09,1};
	soundEscape[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundEscape",0.09,1};
};

class RscDisplaySelectIsland: RscStandardDisplay
{
	class controls
	{
		class ButtonContinue: RscButtonMenuOK
		{
			onButtonClick="[(findDisplay 51),'RscDisplaySelectIsland','continue'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
		};
		class IslandsBox: RscListBox
		{
			onLBDblClick="[(findDisplay 51),'RscDisplaySelectIsland','doubleclick'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
		};
	};
};

class RscDisplayPassword: RscStandardDisplay {
  scriptName = "RscDisplayPassword";
  scriptPath = "GUI";
  onLoad = "[""onLoad"",_this,""RscDisplayPassword"",'GUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  onUnload = "[""onUnload"",_this,""RscDisplayPassword"",'GUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  movingEnable = 0;
  simulationEnabled = 0;
  class controlsbackground {
    class Vignette: RscVignette {
      idc = 114998;
    };
    class TileGroup: RscControlsGroupNoScrollbars {
      idc = 115099;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TitleBackground: RscText {
      idc = 1080;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class MainBackground: RscText {
      idc = 1081;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
  };
  class controls {
    delete B_OK;
    delete B_Cancel;
    class Title: RscTitle {
      idc = 1000;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class PlayersName: Title {
      idc = 601;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TextPassword: RscText {
      idc = 1002;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class ValuePassword: RscEdit {
      idc = 101;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class ButtonOk: RscButtonMenuOK {
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class ButtonCancel: RscButtonMenuCancel {
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
  };
};

class RscDisplayMultiplayerSetup: RscStandardDisplay
{
	onLoad = "[""onLoad"",_this,""RscDisplayMultiplayerSetup"",'GUI'] call compile preprocessfilelinenumbers ""A3\ui_f\scripts\initDisplay.sqf""";
	onUnload = "[""onUnload"",_this,""RscDisplayMultiplayerSetup"",'GUI'] call compile preprocessfilelinenumbers ""A3\ui_f\scripts\initDisplay.sqf""";
	west = "AL_UI\pics\Policia.paa";
	east = "AL_UI\pics\medico.paa";
	guer = "AL_UI\pics\medico.paa";
	Civilian = "AL_UI\pics\civil.paa";
	none = "#(argb,8,8,3)color(0,0,0,0)";
	westUnlocked = "A3\ui_f\data\map\diary\icons\playerWest_ca.paa";
	westLocked = "AL_UI\pics\Policia.paa";
	eastUnlocked = "A3\ui_f\data\map\diary\icons\playerEast_ca.paa";
	eastLocked = "AL_UI\pics\medico.paa";
	guerUnlocked = "A3\ui_f\data\map\diary\icons\playerGuer_ca.paa";
	guerLocked = "AL_UI\pics\medico.paa";
	civlUnlocked = "A3\ui_f\data\map\diary\icons\playerCiv_ca.paa";
	civlLocked = "AL_UI\pics\civil.paa";
	disabledAllAI = "$STR_DISP_MULTI_ENABLE_AI";
	enabledAllAI = "$STR_DISP_MULTI_DISABLE_AI";
	hostLocked = "$STR_DISP_MULTI_UNLOCK";
	hostUnlocked = "$STR_DISP_MULTI_LOCK";
	colorNotAssigned[] = {1,1,1,0.25};
	colorAsaed[] = {1,1,1,1};
	colorConfirmed[] = {0,1,0,1};
	class controlsbackground
	{
		delete RolesBackground;
		delete MissionSettingsBackground;
		delete NumOfPlayersBackground; 
		delete MainBackground;
		delete PlayersPoolHeaderBackground;
		delete PlayersPoolBackground;
		delete ChatBackground;
		delete SideBackground;

		class Vignette: RscVignette
		{
			idc = 114998;
		};
		class TileGroup: RscControlsGroupNoScrollbars
		{
			idc = 115099;
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			h = "safezoneH";
			disableCustomColors = 1;
			class Controls
			{
				class TileFrame: RscFrame
				{
					idc = 114999;
					x = 0;
					y = 0;
					w = "safezoneW";
					h = "safezoneH";
					colortext[] = {0,0,0,1};
				};
				class Tile_0_0: RscText
				{
					idc = 115000;
					x = "(0 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_1: RscText
				{
					idc = 115001;
					x = "(0 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_2: RscText
				{
					idc = 115002;
					x = "(0 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_3: RscText
				{
					idc = 115003;
					x = "(0 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_4: RscText
				{
					idc = 115004;
					x = "(0 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_5: RscText
				{
					idc = 115005;
					x = "(0 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_0: RscText
				{
					idc = 115010;
					x = "(1 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_1: RscText
				{
					idc = 115011;
					x = "(1 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_2: RscText
				{
					idc = 115012;
					x = "(1 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_3: RscText
				{
					idc = 115013;
					x = "(1 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_4: RscText
				{
					idc = 115014;
					x = "(1 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_5: RscText
				{
					idc = 115015;
					x = "(1 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_0: RscText
				{
					idc = 115020;
					x = "(2 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_1: RscText
				{
					idc = 115021;
					x = "(2 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_2: RscText
				{
					idc = 115022;
					x = "(2 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_3: RscText
				{
					idc = 115023;
					x = "(2 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_4: RscText
				{
					idc = 115024;
					x = "(2 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_5: RscText
				{
					idc = 115025;
					x = "(2 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_0: RscText
				{
					idc = 115030;
					x = "(3 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_1: RscText
				{
					idc = 115031;
					x = "(3 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_2: RscText
				{
					idc = 115032;
					x = "(3 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_3: RscText
				{
					idc = 115033;
					x = "(3 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_4: RscText
				{
					idc = 115034;
					x = "(3 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_5: RscText
				{
					idc = 115035;
					x = "(3 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_0: RscText
				{
					idc = 115040;
					x = "(4 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_1: RscText
				{
					idc = 115041;
					x = "(4 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_2: RscText
				{
					idc = 115042;
					x = "(4 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_3: RscText
				{
					idc = 115043;
					x = "(4 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_4: RscText
				{
					idc = 115044;
					x = "(4 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_5: RscText
				{
					idc = 115045;
					x = "(4 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_0: RscText
				{
					idc = 115050;
					x = "(5 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_1: RscText
				{
					idc = 115051;
					x = "(5 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_2: RscText
				{
					idc = 115052;
					x = "(5 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_3: RscText
				{
					idc = 115053;
					x = "(5 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_4: RscText
				{
					idc = 115054;
					x = "(5 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_5: RscText
				{
					idc = 115055;
					x = "(5 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
			};
		};
		/*
		class AL_MainBackG: RscPictureKeepAspect
		{
			idc = -1;
			text = "\AL_UI\pics\lobby_image.paa";
			x = "0.0127 * safezoneW + safezoneX";
			y = "0.177 * safezoneH + safezoneY";
			w = "0.975 * safezoneW";
			h = "0.775 * safezoneH";
		};
		*/
		class RscTitleBackground: RscText
		{
			colorBackground[] = {0,0,0,0.8};
			idc = 1080;
			x = "1 * (((safezoneW / safezoneH) min 1.2) / 40) + (SafezoneX)";
			y = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + (safezoneY)";
			w = "SafezoneW - (2 * (((safezoneW / safezoneH) min 1.2) / 40))";
			h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};

		class AL_WelcomeBackground: RscText
		{
			idc = 1087;
			text = "                                                    Welcome to Platinum-Life Metropolis Roleplay! ";
			x = "0.334999 * safezoneW + safezoneX";
			y = "0.1326 * safezoneH + safezoneY";
			w = "0.397031 * safezoneW";
			h = "0.033 * safezoneH";
			colorBackground[] = {0,0,0,0.8};
		};
		class AL_WhatRoleBackground: RscText
		{
			idc = 1091;
			x = "0.335 * safezoneW + safezoneX";
			y = "0.2206 * safezoneH + safezoneY";
			w = "0.397031 * safezoneW";
			h = "0.165 * safezoneH";
			colorBackground[] = {0,0,0,0.6};
		};
		class AL_WhatRoleTopBackground: RscText
		{
			idc = 1090;
			x = "0.335 * safezoneW + safezoneX";
			y = "0.1986 * safezoneH + safezoneY";
			w = "0.397031 * safezoneW";
			h = "0.022 * safezoneH";
			colorBackground[] = {0,0,0,0.8};
		};
		class AL_PickSlotBackground: RscText
		{
			idc = 1089;
			x = "0.33552 * safezoneW + safezoneX";
			y = "0.451948 * safezoneH + safezoneY";
			w = "0.39651 * safezoneW";
			h = "0.463852 * safezoneH";
			colorBackground[] = {0,0,0,0.6};
		};
		class AL_PickSlotTopBackground: RscText
		{
			idc = 1088;
			x = "0.335516 * safezoneW + safezoneX";
			y = "0.4296 * safezoneH + safezoneY";
			w = "0.39651 * safezoneW";
			h = "0.022 * safezoneH";
			colorBackground[] = {0,0,0,0.8};
		};
		class AL_WhatRoleText: RscText
		{
			idc = 1004;
			text = "What role would you like to play?";
			x = "0.337062 * safezoneW + safezoneX";
			y = "0.1986 * safezoneH + safezoneY";
			w = "0.103125 * safezoneW";
			h = "0.022 * safezoneH";
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.85)";
		};
		class AL_PickSlotText: RscText
		{
			idc = 1010;
			text = "Now, please pick a slot below.";
			x = "0.338093 * safezoneW + safezoneX";
			y = "0.4296 * safezoneH + safezoneY";
			w = "0.103125 * safezoneW";
			h = "0.022 * safezoneH";
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.85)";
		};
	};
	class controls
	{
		delete B_Civilian;
		delete B_Guerrila;
		delete B_East;
		delete B_West;
		delete B_Side;
		delete B_OK;
		delete B_Cancel;
		delete B_Kick;
		delete B_EnableAll;
		delete B_Lock;
		delete TextDescription;
		delete ValueDescription;
		delete TextMessage;
		delete ValueRoles;
		delete TextParam1;
		delete TextParam2;
		delete ValueParam1;
		delete ValueParam2;
		delete ValuePool;
		delete B_Params;
		delete TextPool;
		delete TextRole;
		delete TextSide;
		delete SortPlayers;
		delete ButtonPing;
		delete SortPing;
		delete TextRoles;
		delete CA_B_Params;
		delete CA_B_Lock;
		delete ButtonSteamWorkshop;
		delete CA_B_EnableAll;
		delete CA_B_Kick;
		delete CA_TextVotingTimeLeft;
		delete TextMission;
		delete ValueMission;
		delete CA_TextDescription;
		delete CA_ValueDescription;
		delete TextIsland;
		delete ValueIsland;
		delete TextListedPlayers;
		delete ValueListedPlayers;
		delete MuteAll;

		class Title: RscTitle
		{
			w = "15 * (((safezoneW / safezoneH) min 1.2) / 40) + 0.45*(safezoneW - ((safezoneW / safezoneH) min 1.2))";
			idc = 1000;
			text = "Platinum-Life Metropolis";
			x = "1 * (((safezoneW / safezoneH) min 1.2) / 40) + (SafezoneX)";
			y = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + (safezoneY)";
			h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class PlayersName: RscTitle
		{
			idc = 701;
			style = 1;
			shadow = 0;
			x = "(SafezoneX) + (24 * (((safezoneW / safezoneH) min 1.2) / 40)) + 0.45*(safezoneW - ((safezoneW / safezoneH) min 1.2))";
			w = "(15 * (((safezoneW / safezoneH) min 1.2) / 40)) + 0.55*(safezoneW - ((safezoneW / safezoneH) min 1.2))";
			y = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + (safezoneY)";
			h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class CA_B_West: RscActiveText
		{
			text = " ";
			idc = 104;
			picture = "AL_UI\pics\Policia.paa";
			sideToggle = "AL_UI\pics\Policia.paa";
			sideDisabled = "A3\ui_f\data\gui\rsc\RscDisplayMultiplayerSetup\flag_none_ca.paa";
			pictureHeight = 1;
			pictureWidth = 1;
			color[] = {};
			colorActive[] = {0.22,1,0.28,1};
			colorDisabled[] = {1,1,1,0};
			colorShade[] = {1,1,1,1};
			colorText[] = {1,1,1,1};
			textHeight = 0.25;
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 25)";
			x = "0.355625 * safezoneW + safezoneX";
			y = "0.2448 * safezoneH + safezoneY";
			w = "0.0721875 * safezoneW";
			h = "0.11 * safezoneH";
		};
		class CA_B_Guerrila: CA_B_West
		{
			text = " ";
			idc = 106;
			picture = "AL_UI\pics\medico.paa";
			sideToggle = "AL_UI\pics\medico.paa";
			color[] = {};
			colorActive[] = {0.22,1,0.28,1};
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 25)";
			x = "0.489687 * safezoneW + safezoneX";
			y = "0.2448 * safezoneH + safezoneY";
			w = "0.0721875 * safezoneW";
			h = "0.11 * safezoneH";
		};
		class CA_B_Civilian: CA_B_West
		{
			text = " ";
			idc = 107;
			picture = "AL_UI\pics\civil.paa";
			sideToggle = "AL_UI\pics\civil.paa";
			color[] = {};
			colorActive[] = {0.22,1,0.28,1};
			sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 25)";
			x = "0.634062 * safezoneW + safezoneX";
			y = "0.2448 * safezoneH + safezoneY";
			w = "0.0721875 * safezoneW";
			h = "0.11 * safezoneH";
		};
		class CA_ValuePool: RscListBox
		{
			idc = 114;
			x = 1;
			w = 1;
			h = -2;
			z = -2;
			drawSideArrows = 1;
			idcLeft = 500;
			idcRight = 500;
		};
		class ButtonPlayers: RscButtonTextOnly
		{
			idc = 133;
			style = 0;
			x = 1;
			w = 1;
			h = -2;
			z = -2;
			text = "";
		};
		class CA_ValueRoles: RscListBox
		{
			idc = 109;
			colorPlayer[] = {1,1,0,1};
			colorAI[] = {1,0,0,1};
			colorNobody[] = {1,1,1,0.25};
			drawSideArrows = 1;
			enabledAI = "A3\ui_f\data\gui\rsc\RscDisplayMultiplayerSetup\enabledAI_ca.paa";
			disabledAI = "A3\ui_f\data\gui\rsc\RscDisplayMultiplayerSetup\disabledAI_ca.paa";
			rowHeight = "1.75 * (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
/*			x = "11.5 * (((safezoneW / safezoneH) min 1.2) / 40) + (SafezoneX)";
			w = "(1 * (((safezoneW / safezoneH) min 1.2) / 40)) + 0.45*(safezoneW - ((safezoneW / safezoneH) min 1.2))";
			h = "(7.4 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)) + 0.7*(safezoneH - (((safezoneW / safezoneH) min 1.2) / 1.2))";
			y = "9 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + (safezoneY)";*/
			x = "0.340156 * safezoneW + safezoneX";
			y = "0.4582 * safezoneH + safezoneY";
			w = "0.387135 * safezoneW";
			h = "0.449629 * safezoneH";
			colorSelectBackground[] = {0,0.09,0.57,0.4};
			colorSelectBackground2[] = {0,0.09,0.57,0.4};
			colorBackground[] = {-1,-1,-1,-1};
		};
		class CA_ButtonContinue: RscButtonMenuOK
		{
			x = "0.336031 * safezoneW + safezoneX";
			y = "0.929 * safezoneH + safezoneY";
			w = "0.397031 * safezoneW";
			h = "0.022 * safezoneH";
		};
		class CA_ButtonCancel: RscButtonMenuCancel
		{
			text = "$STR_DISP_BACK";
			x = "0.336031 * safezoneW + safezoneX";
			y = "0.9598 * safezoneH + safezoneY";
			w = "0.397031 * safezoneW";
			h = "0.022 * safezoneH";
		};
	};
};


class RscDisplayMultiplayer: RscStandardDisplay {
  scriptName = "RscDisplayMultiplayer";
  scriptPath = "GUI";
  onLoad = "[""onLoad"",_this,""RscDisplayMultiplayer"",'GUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  onUnload = "[""onUnload"",_this,""RscDisplayMultiplayer"",'GUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  sortUp = "";
  sortDown = "";
  showPassworded = "";
  hidePassworded = "";
  showBattlEye = "";
  showNoBattlEye = "";
  hideBattlEye = "";
  showFull = "";
  hideFull = "";
  showExpansions = "";
  hideExpansions = "";
  modPresent = "";
  modMissing = "";
  colorPingUnknown[] = {0.4,0.4,0.4,1};
  colorPingGood[] = {0,1,0,1};
  colorPingPoor[] = {1,0.6,0,1};
  colorPingBad[] = {1,0,0,1};
  colorVersionGood[] = {1,1,1,1};
  colorVersionBad[] = {1,0,0,1};
  class controlsbackground {
    delete MainbackTop;
    delete MainbackBottom;
    delete MainbackMiddle;
    delete CA_ServerDetailLanguage;
    delete CA_ServerDetailMission;
    delete CA_ServerDetailPing;
    delete CA_ServerDetailSlots;
    delete CA_ServerDetailState;
    delete CA_TextDetailState;
    delete CA_TextServerDetailLanguage;
    delete CA_TextServerDetailPing;
    delete CA_TextServerDetailSlots;
    class Vignette: RscVignette {
      idc = 114998;
    };
    class TileGroup: RscControlsGroupNoScrollbars {
      idc = 115099;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
      class Controls {
        class Background: RscText {
          idc = 114999;
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
      };
    };
    class RscTitleBackground: RscText {
      idc = 1080;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class MainBackground: RscText {
      idc = 1081;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabsBackground: RscText {
      idc = 1082;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class ServerTabsBackground: RscText {
      idc = 1083;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class HeadersBackground: RscText {
      idc = 1084;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
  };
  class controls {
    delete Title;
    delete ValueSessions;
    delete ColumnPlayers;
    delete ColumnPing;
    delete ColumnType;
    delete ColumnMission;
    delete ColumnState;
    delete ColumnServer;
    delete FilterPlayers;
    delete FilterPing;
    delete FilterMission;
    delete FilterType;
    delete FilterServer;
    delete ServerDetailType;
    delete ServerDetailMission;
    delete ServerDetailState;
    delete ServerDetailSlots;
    delete ServerDetailPing;
    delete ServerDetailLanguage;
    delete ServerDetailCountry;
    delete ServerDetailVersion;
    delete ServerDetailVersionRequired;
    delete ServerDetailMods;
    delete ServerDetailIsland;
    delete ServerDetailDifficulty;
    delete ServerDetailTimeLeft;
    delete ServerDetailPlatform;
    delete ServerDetailPlayers;
    delete ServerDetailPassword;
    delete ServerDetailHost;
    delete FilterBattlEyeServers;
    delete FilterPasswordedServers;
    delete FilterFullServers;
    delete IconPlayers;
    delete IconPing;
    delete IconType;
    delete IconState;
    delete IconMission;
    delete IconServer;
    delete B_DPlay;
    delete B_Internet;
    delete B_Remote;
    delete B_Password;
    delete B_Refresh;
    delete B_Filter;
    delete B_Port;
    delete Progress;
    delete B_Join;
    delete B_New;
    delete B_Cancel;
    delete CA_Internet_Lan;
    class CA_FramePlayers: RscFrame {
      idc = 1801;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_FrameExpansions: RscFrame {
      idc = 1802;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_FrameMission: RscFrame {
      idc = 1803;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailPassword: RscPicture {
      idc = 143;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailHost: RscText {
      idc = 129;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TextServerDetailIsland: RscText {
      idc = 1019;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailIsland: RscText {
      idc = 132;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TextServerDetailDifficulty: CA_TextServerDetailIsland {
      idc = 1021;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailDifficulty: CA_ServerDetailIsland {
      idc = 138;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TextServerDetailPlatform: CA_TextServerDetailIsland {
      idc = 1014;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailPlatform: CA_ServerDetailIsland {
      idc = 130;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TextServerDetailCountry: CA_TextServerDetailIsland {
      idc = 1012;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailCountry: CA_ServerDetailIsland {
      idc = 145;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TextServerDetailBE: CA_TextServerDetailIsland {
      idc = 1010;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailBE: CA_ServerDetailIsland {
      idc = 201;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TextServerDetailFilepatching: CA_TextServerDetailIsland {
      idc = 1025;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class ServerDetailFilepatching: CA_ServerDetailIsland {
      idc = 169;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TextServerDetailTimeLeft: CA_TextServerDetailIsland {
      idc = 1017;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailTimeLeft: CA_ServerDetailIsland {
      idc = 134;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TextServerType: CA_TextServerDetailIsland {
      idc = 1007;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailVersion: CA_ServerDetailIsland {
      idc = 144;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailVersionRequired: CA_ServerDetailIsland {
      idc = 147;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailExpansion: RscListBox {
      idc = 148;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerDetailPlayers: RscListBox {
      idc = 149;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_Cancel: RscButtonMenuCancel {
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_Refresh: CA_Cancel {
      idc = 123;
      shortcuts[] = {"0x00050000 + 3",63};
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_Join: RscButtonMenuOK {
      idc = 105;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_BFilter: CA_Cancel {
      idc = 124;
      shortcuts[] = {};
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_Title2: RscTitle {
      idc = 1000;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class PlayersName: RscTitle {
      idc = 1008;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_FilterPasswordedServers: RscPicture {
      idc = 150;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_FilterFullServers: CA_FilterPasswordedServers {
      idc = 151;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerBEFilter: CA_FilterPasswordedServers {
      idc = 154;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerExpansionsFilter: CA_FilterPasswordedServers {
      idc = 155;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabQuickPlay: CA_Cancel {
      idc = 164;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabServers: CA_Cancel {
      idc = 165;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_BRemote: CA_Cancel {
      idc = 166;
      shortcuts[] = {};
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_New: CA_Cancel {
      idc = 167;
      shortcuts[] = {"0x00050000 + 2"};
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerCount: RscText {
      idc = 158;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabRecent: RscButtonMenu {
      idc = 160;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabFriends: RscButtonMenu {
      idc = 162;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabOfficialServers: RscButtonMenu {
      idc = 163;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabLAN: RscButtonMenu {
      idc = 161;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabInternet: RscButtonMenu {
      idc = 159;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_FavoriteColumn: RscShortcutButton {
      idc = 156;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerColumn: RscButtonTextOnly {
      idc = 112;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TypeColumn: CA_ServerColumn {
      idc = 141;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_MissionColumn: CA_ServerColumn {
      idc = 114;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_StateColumn: CA_ServerColumn {
      idc = 116;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_PlayersColumn: CA_ServerColumn {
      idc = 118;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_PingColumn: CA_ServerColumn {
      idc = 120;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_FavoriteIcon: RscPicture {
      idc = 157;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ServerIcon: RscPicture {
      idc = 111;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_TypeIcon: CA_ServerIcon {
      idc = 140;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_MissionIcon: CA_ServerIcon {
      idc = 113;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_StateIcon: CA_ServerIcon {
      idc = 115;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_PlayersIcon: CA_ServerIcon {
      idc = 117;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_PingIcon: CA_ServerIcon {
      idc = 119;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class CA_ValueSessions: RscListBox {
      idc = 102;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
      class Columns {
        class ColumnFavorite {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ColumnServer {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ColumnType {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ColumnMission {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ColumnState {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ColumnPlayers {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ColumnPing {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
      };
    };
    class ButtonLauncherServerBrowser: RscButtonMenu {
      idc = 168;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
  };
};

class RscDisplayIPAddress: RscStandardDisplay {
  scriptName = "RscDisplayIPAddress";
  scriptPath = "GUI";
  onLoad = "[""onLoad"",_this,""RscDisplayIPAddress"",'GUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  onUnload = "[""onUnload"",_this,""RscDisplayIPAddress"",'GUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  class controlsBackground {
    class Vignette: RscVignette {
      idc = 114998;
    };
    class TileGroup: RscControlsGroupNoScrollbars {
      idc = 115099;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
      class Controls {
        class Background: RscText {
          idc = 114999;
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
      };
    };
    class TitleBackground: RscText {
      idc = 1080;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class MainBackground: RscText {
      idc = 1081;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabsBackground: RscText {
      idc = 1082;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
  };
  class controls {
    delete B_OK;
    delete B_Cancel;
    delete TextPort;
    delete ValuePort;
    delete TextAddress;
    delete ValueAddress;
    class Title: RscTitle {
      idc = 1000;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class PlayersName: Title {
      idc = 1001;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabQuickPlay: RscButtonMenu {
      idc = 164;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabServers: RscButtonMenu {
      idc = 165;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabDirectConnect: RscButtonMenu {
      idc = 166;
      shortcuts[] = {};
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class TabSetupServer: RscButtonMenu {
      idc = 167;
      shortcuts[] = {"0x00050000 + 2"};
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class Content: RscControlsGroupNoScrollbars {
      idc = 2300;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
      class controls {
        class CA_TextAddress: RscText {
          idc = 1003;
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class CA_ValueAddress: RscEdit {
          idc = 101;
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class CA_TextPort: CA_TextAddress {
          idc = 1004;
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class CA_ValuePort: CA_ValueAddress {
          idc = 102;
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
        class ButtonOk: RscButtonMenuOK {
          w = 1;
          h = 1;
          x = -2;
          y = -2;
        };
      };
    };
    class ButtonLauncherServerBrowser: RscButtonMenuOK {
      idc = 168;
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
    class ButtonBack: RscButtonMenuCancel {
      w = 1;
      h = 1;
      x = -2;
      y = -2;
    };
  };
};

class RscDisplayMain: RscStandardDisplay
{
	idd=0;
	onLoad="[_this,'RscDisplayMain','load'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
	onUnload="[_this,'RscDisplayMain','unload'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
	class Spotlight
	{
		class Bootcamp
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class EastWind
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class ApexProtocol
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
	};
	class ControlsBackground
	{
		delete MouseArea;
		delete BackgroundLeft;
		delete BackgroundRight;
		delete Picture;
	};
	class Controls
	{
		class ModIcons;
		class B_Quit;
		class B_Expansions;
		class B_Credits;
		class B_Player;
		class B_Options;
		class B_SinglePlayer;
		class B_MissionEditor;
		class B_MultiPlayer;
		class B_SingleMission;
		class B_Campaign;
		class Date;
		class ModList;
		class TrafficLight;
		class Version;
		class BackgroundSpotlight: RscPicture
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class BackgroundSpotlightLeft: BackgroundSpotlight
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class BackgroundSpotlightRight: BackgroundSpotlightLeft
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class SpotlightBase: RscControlsGroupNoScrollbars
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class Spotlight1: SpotlightBase
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class Spotlight2: SpotlightBase
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class Spotlight3: SpotlightBase
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class BackgroundBar: RscText
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class BackgroundCenter: BackgroundBar
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class BackgroundBarLeft: RscPicture
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class BackgroundBarRight: BackgroundBarLeft
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class Logo: RscActivePicture
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class LogoApex: Logo
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleSingleplayer: RscButtonMenu
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleIconSingleplayer: RscButton
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class GroupSingleplayer: RscControlsGroupNoScrollbars
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleMultiplayer: TitleSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleIconMultiplayer: TitleIconSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class GroupMultiplayer: GroupSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleTutorials: TitleSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleIconTutorials: TitleIconSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class GroupTutorials: GroupSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleOptions: TitleSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleIconOptions: TitleIconSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class GroupOptions: GroupSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class TitleSession: RscButton
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class GroupSession: GroupSingleplayer
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class Exit: RscButton
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class InfoMods: RscControlsGroupNoHScrollbars
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class InfoDLCsOwned: InfoMods
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class InfoDLCs: InfoDLCsOwned
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class InfoNews: InfoMods
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class InfoVersion: InfoNews
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class Footer: RscText
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class AllMissions: RscButtonMenuMain
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
		class ProofsOfConcept: AllMissions
		{
			w=1;
			h=1;
			x=-2;
			y=-2;
		};
	};
	class IconPicture: RscPictureKeepAspect
	{
		w=1;
		h=1;
		x=-2;
		y=-2;
	};
	class DlcOwnedIconPicture: RscPictureKeepAspect
	{
		w=1;
		h=1;
		x=-2;
		y=-2;
	};
	class DlcIconPicture: RscPictureKeepAspect
	{
		w=1;
		h=1;
		x=-2;
		y=-2;
	};
};

class AL_ModsMissing
{
	idd=6700;
	movingEnable=0;
	enableSimulation=1;
	  
	class ControlsBackground
	{
		class Background: RscText
		{
		    idc = 1000;
		    x = 0.323716 * safezoneW + safezoneX;
		    y = 0.377813 * safezoneH + safezoneY;
		    w = 0.352567 * safezoneW;
		    h = 0.244374 * safezoneH;
		    colorBackground[] = {-1,-1,-1,0.9};
		};
		class RscText_1002: RscStructuredText
		{
		    idc = 1002;
		    text = "<t align='center'>Our mod pack has been updated and you now need to download one of our mods from the Steam workshop.</t>"; //--- ToDo: Localize;
		    x = 0.328123 * safezoneW + safezoneX;
		    y = 0.434207 * safezoneH + safezoneY;
		    w = 0.343753 * safezoneW;
		    h = 0.0563941 * safezoneH;
		};
		class RscText_1003: RscStructuredText
		{
		    idc = 1003;
		    text = "<t align='center'>You just need the mod itself. All dependencies for it are already in our mod pack and you can ignore them.</t>"; //--- ToDo: Localize;
		    x = 0.328123 * safezoneW + safezoneX;
		    y = 0.471803 * safezoneH + safezoneY;
		    w = 0.343753 * safezoneW;
		    h = 0.0469951 * safezoneH;
		};
		class RscText_1004: RscText
		{
		    idc = 1004;
		    text = "You can download the mod here:"; //--- ToDo: Localize;
		    x = 0.33253 * safezoneW + safezoneX;
		    y = 0.537596 * safezoneH + safezoneY;
		    w = 0.334939 * safezoneW;
		    h = 0.0281971 * safezoneH;
		};
	};

	class Controls
	{
		class RscText_1001: RscStructuredText
	    {
	        idc = 1001;
	        text = "<t size='1.5' color='#FF3737' align='center'>Oops! Seems like you're missing some mods!</t>"; //--- ToDo: Localize;
	        x = 0.328123 * safezoneW + safezoneX;
	        y = 0.387212 * safezoneH + safezoneY;
	        w = 0.343753 * safezoneW;
	        h = 0.0375961 * safezoneH;
	    };
		class ModLink: RscEdit
	    {
	        idc = 1400;
	        text = "https://steamcommunity.com/sharedfiles/filedetails/?id=402601958"; //--- ToDo: Localize;
	        x = 0.33253 * safezoneW + safezoneX;
	        y = 0.565793 * safezoneH + safezoneY;
	        w = 0.334939 * safezoneW;
	        h = 0.0281971 * safezoneH;
			canModify = false;
	    };
	};
};

class AL_MainMenu
{
	idd=6600;
	movingEnable=0;
	enableSimulation=1;
	onLoad="[""onLoad"",_this,""AL_MainMenu"",'GUI'] call compile preprocessfilelinenumbers ""\AL_UI\Scripts\GUI\RscDisplayMain.sqf""; call compile preprocessfilelinenumbers ""\AL_UI\Scripts\addoncheck.sqf""";
  
  class ControlsBackground
  {
    class MouseArea: RscText
    {
      idc = 999;
      style = 16;
      x = "safezoneXAbs";
      y = "safezoneY";
      w = "safezoneWAbs";
      h = "safezoneH";
    };
  };

	class Controls
	{
	class BackgroundBar: RscText
	{
		colorBackground[] = {0,0,0,0.75};
		x = "safezoneX";
		y = "safezoneY + 2 * 	(pixelH * pixelGrid * 2)";
		w = "safezoneW";
		h = "2 * 	(pixelH * pixelGrid * 2)";
	};
	class BackgroundCenter: BackgroundBar
	{
		colorBackground[] = {0,0,0,1};
		x = "0.5 - 2 * 		2 * 	(pixelW * pixelGrid * 2)";
		w = "4 * 		2 * 	(pixelW * pixelGrid * 2)";
	};
	class BackgroundBarLeft: RscPicture
	{
		text = "\a3\Ui_f\data\GUI\Rsc\RscDisplayMain\gradientMods_ca.paa";
		colorText[] = {0,0,0,1};
		angle = 180;
		x = "0.5 - 4 * 		2 * 	(pixelW * pixelGrid * 2)";
		y = "safezoneY + 2 * 	(pixelH * pixelGrid * 2)";
		w = "2 * 		2 * 	(pixelW * pixelGrid * 2)";
		h = "2 * 	(pixelH * pixelGrid * 2)";
	};
	class BackgroundBarRight: BackgroundBarLeft
	{
		angle = 0;
		x = "0.5 + 2 * 		2 * 	(pixelW * pixelGrid * 2)";
	};
    class Logo: RscActivePicture
    {
      text = "\AL_UI\pics\armalifelogo1.paa";
      tooltip = "";
      color[] = {0.9,0.9,0.9,1};
      colorActive[] = {1,1,1,1};
      shadow = 0;
      x = "0.5 -  5 *   (pixelW * pixelGrid * 2)";
      y = "safezoneY + (3 - 0.5 *   5) *  (pixelH * pixelGrid * 2)";
      w = "2 *  5 *   (pixelW * pixelGrid * 2)";
      h = "1 *  5 *   (pixelH * pixelGrid * 2)";
      url = "https://a3platinum-life.com/";
      onButtonClick = "";
      onSetFocus = "(_this select 0) ctrlsettextcolor [1,1,1,1];";
      onKillFocus = "(_this select 0) ctrlsettextcolor [0.9,0.9,0.9,1];";
      onLoad = "(_this select 0) ctrlshow !(395180 in getDLCs 1)";
    };
    class LogoApex: Logo
    {
      text = "\AL_UI\pics\armalifelogo1.paa";
      x = "0.5 -  6.25 *  (pixelW * pixelGrid * 2)";
      y = "safezoneY + (3 - 0.37 *  6.25) *   (pixelH * pixelGrid * 2)";
      w = "2 *  6.25 *  (pixelW * pixelGrid * 2)";
      h = "1 *  6.25 *  (pixelH * pixelGrid * 2)";
      show = 1;
      onLoad = "(_this select 0) ctrlshow (395180 in getDLCs 1)";
    };
    class TitleSingleplayer: RscButtonMenu
    {
      idc = 1011;
      text = "Play Platinum-Life";
      size = "1.25 *  (pixelH * pixelGrid * 2)";
      style = "0x02";
      colorBackground[] = {0,0,0,0};
      colorBackground2[] = {1,1,1,1};
      class Attributes
      {
        align = "center";
        color = "#ffffff";
        font = "PuristaLight";
        shadow = 0;
        size = 1;
      };
      class TextPos
      {
        left = "0.1 *     2 *   (pixelW * pixelGrid * 2)";
        top = "0.18 *     2 *   (pixelH * pixelGrid * 2)";
        right = "0.1 *    2 *   (pixelW * pixelGrid * 2)";
        bottom = "0.18 *    2 *   (pixelH * pixelGrid * 2)";
      };
      x = "0.5 - (  5 + 2 *   10) *   (pixelW * pixelGrid * 2)";
      y = "safezoneY + 2 *  (pixelH * pixelGrid * 2)";
      w = "10 *   (pixelW * pixelGrid * 2)";
      h = "2 *  (pixelH * pixelGrid * 2)";
    };
    class GroupSingleplayer: RscControlsGroupNoScrollbars
    {
      idc = 1001;
      x = "0.5 - (  5 + 2 *   10) *   (pixelW * pixelGrid * 2)";
      y = "safezoneY + (2 +     2) *  (pixelH * pixelGrid * 2)";
      w = "10 *   (pixelW * pixelGrid * 2)";
      h = "(5 *   1.5) *  (pixelH * pixelGrid * 2)";
      class Controls
      {
        class Server1: RscButtonMenuMain
        {
          idc = 1251;
          text = "Play Main Server";
          tooltip = "Play Platinum-Life Server";
          x = 0;
          y = "(0) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          w = "10 *   (pixelW * pixelGrid * 2)";
          h = "1.5 *  (pixelH * pixelGrid * 2) -  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu','playserver1'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
        };
        class TestServer: Server1
        {
          idc = 1521;
          text = "Test Server";
          tooltip = "Test Server";
          y = "(1 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu','testserver2'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
          show = 0;
          onLoad = "(_this select 0) ctrlshow (profileName isEqualTo ""Dezso Kovi"" || profileName isEqualTo ""Joe Casper"" || profileName isEqualTo ""Jack Port"" || profileName isEqualTo ""Vincent North"")";
        };
        class LocalCompServer: TestServer
        {
          idc = 1511;
          text = "Offline";
          tooltip = "Test Server";
          y = "(2 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu','testserver1'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
          show = 0;
          onLoad = "(_this select 0) ctrlshow (profileName isEqualTo ""Dezso Kovi"" || profileName isEqualTo ""Joe Casper"")";
        };
        class LocalServer: TestServer
        {
          idc = 1511;
          text = "Offline";
          tooltip = "Test Server";
          y = "(3 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu','localserver'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
          show = 0;
          onLoad = "(_this select 0) ctrlshow (profileName isEqualTo ""Dezso Kovi"" || profileName isEqualTo ""Joe Casper"")";
        };
      };
    };
    class TitleMultiplayer: TitleSingleplayer
    {
      idc = 1012;
      text = "Visit Editor";
      x = "0.5 - (  5 +   10) *   (pixelW * pixelGrid * 2)";
      onButtonClick = "if (profileName isEqualTo ""Dezso Kovi"" || profileName isEqualTo ""Joe Casper"" || profileName isEqualTo ""Jack Port"" || profileName isEqualTo ""Vincent North"") then {[(findDisplay 6600),'AL_MainMenu','freeAction'] call (uiNamespace getVariable 'AL_fnc_initDisplay')} else {systemChat ""Not Authorized!""};";
    };

    class TitleTutorials: TitleSingleplayer
    {
      idc = 203;
      text = "Virtual Arsenal";
      x = "0.5 + (  5) *  (pixelW * pixelGrid * 2)";
      onButtonClick = "if (profileName isEqualTo ""Dezso Kovi"" || profileName isEqualTo ""Joe Casper"" || profileName isEqualTo ""Jack Port"" || profileName isEqualTo ""Vincent North"") then {[(findDisplay 6600),'AL_MainMenu','arsenal'] call (uiNamespace getVariable 'AL_fnc_initDisplay')} else {systemChat ""Not Authorized!""};";
    };
    class TitleOptions: TitleSingleplayer
    {
      idc = 1014;
      text = "$STR_A3_RscDisplayMain_TitleOptions_text";
      x = "0.5 + (  5 +   10) *   (pixelW * pixelGrid * 2)";
    };
    class GroupOptions: GroupSingleplayer
    {
      idc = 1004;
      x = "0.5 + (  5 +   10) *   (pixelW * pixelGrid * 2)";
      h = "(5 *   1.5) *  (pixelH * pixelGrid * 2)";
      class Controls: Controls
      {
        class Video: Server1
        {
          idc = 301;
          text = "$STR_A3_RscDisplayMain_ButtonVideo";
          tooltip = "$STR_TOOLTIP_MAIN_VIDEO";
          y = "(0 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu_Options','video'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
        };
        class Audio: Server1
        {
          idc = 302;
          text = "$STR_A3_RscDisplayMain_ButtonAudio";
          tooltip = "$STR_TOOLTIP_MAIN_AUDIO";
          y = "(1 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu_Options','audio'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
        };
        class Controls: Server1
        {
          idc = 303;
          text = "$STR_A3_RscDisplayMain_ButtonControls";
          tooltip = "$STR_TOOLTIP_MAIN_CONTROLS";
          y = "(2 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu_Options','keybindings'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
        };
        class Game: Server1
        {
          idc = 307;
          text = "$STR_A3_RscDisplayMain_ButtonGame";
          tooltip = "$STR_TOOLTIP_MAIN_GAME";
          y = "(3 *   1.5) *  (pixelH * pixelGrid * 2) +  (pixelH)";
          onButtonClick = "[(findDisplay 6600),'AL_MainMenu_Options','game'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
        };
      };
    };

    class TitleSession: RscButton
    {
      idc = 1015;
      text = "\a3\Ui_f\data\GUI\Rsc\RscDisplayMain\profile_player_ca.paa";
      style = 48;
      colorText[] = {1,1,1,0.5};
      colorBackground[] = {0,0,0,0};
      colorBackgroundActive[] = {1,1,1,1};
      colorFocused[] = {0,0,0,0};
      onButtonClick = "[(findDisplay 6600),'AL_MainMenu','profiles'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
      x = "safezoneX + safezoneW - 2 *    2 *   (pixelW * pixelGrid * 2)";
      y = "safezoneY + 2 *  (pixelH * pixelGrid * 2)";
      w = "2 *  (pixelW * pixelGrid * 2)";
      h = "2 *  (pixelH * pixelGrid * 2)";
      onMouseEnter = "(_this select 0) ctrlsettextcolor [0,0,0,1];";
      onSetFocus = "(_this select 0) ctrlsettextcolor [0,0,0,1];";
      onMouseExit = "(_this select 0) ctrlsettextcolor [1,1,1,0.5];";
      onKillFocus = "(_this select 0) ctrlsettextcolor [1,1,1,0.5];";
    };

    class Exit: RscButton
    {
      idc = 106;
      text = "\a3\Ui_f\data\GUI\Rsc\RscDisplayMain\exit_ca.paa";
      tooltip = "Quit Game?";
      style = 48;
      colorText[] = {1,1,1,0.5};
      colorBackground[] = {0,0,0,0};
      colorBackgroundActive[] = {0.784314,0.137255,0.0627451,1};
      colorFocused[] = {1,1,1,1};
      onButtonClick = "[(findDisplay 6600),'AL_MainMenu','exit'] call (uiNamespace getVariable 'AL_fnc_initDisplay')";
      x = "safezoneX + safezoneW -    2 *   (pixelW * pixelGrid * 2)";
      y = "safezoneY + 2 *  (pixelH * pixelGrid * 2)";
      w = "2 *  (pixelW * pixelGrid * 2)";
      h = "2 *  (pixelH * pixelGrid * 2)";
      onMouseEnter = "(_this select 0) ctrlsettextcolor [1,1,1,1];";
      onSetFocus = "(_this select 0) ctrlsettextcolor [1,1,1,1];";
      onMouseExit = "(_this select 0) ctrlsettextcolor [1,1,1,0.5];";
      onKillFocus = "(_this select 0) ctrlsettextcolor [1,1,1,0.5];";
    };

    class Footer: RscText
    {
      text = "© 2018 Platinum-life. All Rights Reserved.";
      style = 2;
      sizeEx = "1 * (pixelH * pixelGrid * 2)";
      shadow = 0;
      font = "RobotoCondensedLight";
      colorText[] = {1,1,1,0.5};
      colorBackground[] = {0,0,0,0.75};
      x = "safezoneX";
      y = "safezoneY + safezoneH - 1 *  1 *   (pixelH * pixelGrid * 2)";
      w = "safezoneW";
      h = "(  1) *  (pixelH * pixelGrid * 2)";
    };
	};
};

class RscBackgroundLogo: RscPictureKeepAspect
{
	text = "\AL_UI\pics\AL_logo_513.paa";
	x = "0.33375 * safezoneW";
	y = "0.29 * safezoneH";
	w = "0.3325 * safezoneW";
	h = "0.39375 * safezoneH";
};

class RscDisplayStart: RscStandardDisplay
{
	class controls
	{
		class LoadingStart: RscControlsGroup
		{
			class controls
			{
				class Logo: RscPictureKeepAspect
				{
					text = "\AL_UI\pics\AL_logo_513.paa";
					x = "0.33375 * safezoneW";
					y = "0.29 * safezoneH";
					w = "0.3325 * safezoneW";
					h = "0.39375 * safezoneH";
					onLoad = "";
				};
			};
		};
	};
};
class RscDisplayNotFreeze: RscStandardDisplay
{
	class controls
	{
		class LoadingStart: RscControlsGroup
		{
			class controls
			{
				class Logo: RscPictureKeepAspect
				{
					text = "\AL_UI\pics\AL_logo_513.paa";
					x = "0.33375 * safezoneW";
					y = "0.29 * safezoneH";
					w = "0.3325 * safezoneW";
					h = "0.39375 * safezoneH";
					onLoad = "";
				};
			};
		};
	};
};
class RscDisplayLoadMission: RscStandardDisplay
{
	class controls
	{
		class LoadingStart: RscControlsGroup
		{
			class controls
			{
				class Logo: RscPictureKeepAspect
				{
					text = "\AL_UI\pics\AL_logo_513.paa";
					x = "0.33375 * safezoneW";
					y = "0.29 * safezoneH";
					w = "0.3325 * safezoneW";
					h = "0.39375 * safezoneH";
					onLoad = "";
				};
			};
		};
	};
};

class RscTitles
{
	class SplashArma3
	{
		class Picture: RscPictureKeepAspect
		{
			text = "\AL_UI\pics\AL_logo_513.paa";
			x = "0.33375 * safezoneW";
			y = "0.29 * safezoneH";
			w = "0.3325 * safezoneW";
			h = "0.39375 * safezoneH";
		};
	};
};