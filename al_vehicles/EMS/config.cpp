class defaultUserActions;
class DefaultEventhandlers;
class CfgPatches
{
	class AL_Air
	{
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"A3_Soft_F"
		};
		units[]=
		{
			"al_EXPLORER_CHIEF",
			"al_EXPLORER_CHIEF_EMS",
			"al_EXPLORER_DEPUTYCHIEF",
			"al_EXPLORER_DEPUTYCHIEF_EMS",
			"al_EXPLORER_ASSCHIEF",
			"al_EXPLORER_ASSCHIEF_EMS",
			"al_EXPLORER_EMSCPT",
			"al_EXPLORER_EMSLT",
			"al_EXPLORER_FTO",
			"al_EXPLORER_EMS",
			"al_TAURUS_EMS",
			"al_TAURUS_VOL",
			"al_TAURUS_SRLT",
			"al_TAURUS_SRCPT",
			"al_TAURUS_EMSFTO",
			"al_VAN_EMS",
			"al_CHARGER_EMS",
			"al_CHARGER_VOLUNTEER",
			"al_CHARGER_CHIEF",
			"al_CHARGER_ASSCHIEF",
			"al_CHARGER_DEPCHIEF",
			"al_CHARGER_CAPTAIN",
			"al_CHARGER_LIEUTENANT",
			"al_SAVANA_EMS",
			"al_MUSTANG_EMS",
			"al_DURANGO_EMS",
			"al_DURANGO_ASSISTANT",
			"al_DURANGO_DEPUTY",
			"al_DURANGO_CHIEF",
			"al_TAHOE_EMS",
			"al_ORCA_EMS",
			"al_EMS_BMW_BR",
			"AL_EMS_RANGEROVER",
			"al_EMS_BMW_WR"

		};
		weapons[]={};
	};
};
class CfgVehicles
{
	class d3s_nemises_explorer_EMS_13;
	class al_EXPLORER_CHIEF: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] Chief Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\chief.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_CHIEF_EMS: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] Chief Ford Explorer (EMS Style)";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\emschiefexplorer.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_DEPUTYCHIEF: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] Deputy Chief Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\depchief.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_DEPUTYCHIEF_EMS: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] Deputy Chief Ford Explorer (EMS Style)";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\depchiefexplorerems.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_ASSCHIEF: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] Ast. Chief Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\asschief.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_ASSCHIEF_EMS: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] Ast. Chief Ford Explorer (EMS Style)";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\asschiefexplorerems.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_EMSCPT: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] CPT Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\ecptexplorer.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_EMSLT: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] EMS LT Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\eltexplorer.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_FTO: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] FTO Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\fto.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EXPLORER_EMS: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[FD] EMS Ford Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\explorer.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_nemises_taurus_FPI_10;
	class al_TAURUS_EMS: d3s_nemises_taurus_FPI_10
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] EMS Ford Taurus";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\taurus.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_TAURUS_VOL: d3s_nemises_taurus_FPI_10
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Volunteer Ford Taurus";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\vol.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_TAURUS_SRLT: d3s_nemises_taurus_FPI_10
	{
		author="Nicole";
		scope=2;
		displayName="[FD] LT Ford Taurus";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\srlt.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_svr_17_COP;
	class AL_EMS_RANGEROVER: d3s_svr_17_COP
	{
		author="George Matthews";
		scope=2;
		displayName="[EMS] Range Rover";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\EMSRangerRover.paa",
			"#(argb,8,8,3)color(0.501961,0.501961,0.501961,1.0,co)",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class al_TAURUS_SRCPT: d3s_nemises_taurus_FPI_10
	{
		author="Nicole";
		scope=2;
		displayName="[FD] CPT Ford Taurus";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\srcpt.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_TAURUS_EMSFTO: d3s_nemises_taurus_FPI_10
	{
		author="Nicole";
		scope=2;
		displayName="[FD] FTO Ford Taurus";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\ftotaurus.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_nemises_vklasse_17_EMS;
	class al_VAN_EMS: d3s_nemises_vklasse_17_EMS
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] EMS Mercedez Van";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\van.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_charger_15_CPP;
	class al_CHARGER_EMS: d3s_charger_15_CPP
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] EMS Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\charger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_CHARGER_VOLUNTEER: d3s_charger_15_CPP
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Volunteer Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\volcharger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_CHARGER_CHIEF: d3s_charger_15_CPP
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Chief Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\chiefcharger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_CHARGER_ASSCHIEF: d3s_charger_15_CPP
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Asst. Chief Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\asscharger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_CHARGER_DEPCHIEF: d3s_charger_15_CPP
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Dep. Chief Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\depcharger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_CHARGER_CAPTAIN: d3s_charger_15_CPP
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] Captain Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\chargercaptain.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_CHARGER_LIEUTENANT: d3s_charger_15_CPP
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] Lieutenant Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\chargerlieutenant.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_nemises_savana_EMS;
	class al_SAVANA_EMS: d3s_nemises_savana_EMS
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] EMS GMC Savana";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\sav.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_boss_15_COP;
	class al_MUSTANG_EMS: d3s_boss_15_COP
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] EMS Mustang";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\mustang.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_durango_18_DDPI;
	class al_DURANGO_EMS: d3s_durango_18_DDPI
	{
		author="George Matthews";
		scope=2;
		displayName="[FD] SAR Durango";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\durango.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_DURANGO_ASSISTANT: d3s_durango_18_DDPI
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Asst. Chief Durango";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\assistantdurango.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_DURANGO_DEPUTY: d3s_durango_18_DDPI
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Dept. Chief Durango";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\deputydurango.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_DURANGO_CHIEF: d3s_durango_18_DDPI
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Chief Durango";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\chiefdurango.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_tahoe_PPV_2;
	class al_TAHOE_EMS: d3s_tahoe_PPV_2
	{
		author="Nicole";
		scope=2;
		displayName="[FD] EMS Tahoe";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\emstahoe.paa",
			"d3s_tahoe_08\data\Tahoe_wheel.paa",
			"d3s_tahoe_08\data\Tahoe_windows.paa"
		};
	};
	class d3s_nemises_orca_EMS;
	class al_ORCA_EMS: d3s_nemises_orca_EMS
	{
		author="Nicole";
		scope=2;
		displayName="[FD] Orca Helicopter";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\orca.paa"
		};
	};
	class d3s_nemises_F90_18_PD;
	class al_EMS_BMW_BR: d3s_nemises_F90_18_PD
	{
		author="Nicole";
		scope=2;
		displayName="[FD] BMW B&R";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\BMWxblack.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class al_EMS_BMW_WR: d3s_nemises_F90_18_PD
	{
		author="Nicole";
		scope=2;
		displayName="[FD] BMW W&R";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\EMS\data\bwmxwhite.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
};
