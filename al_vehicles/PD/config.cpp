class defaultUserActions;
class DefaultEventhandlers;
class CfgPatches
{
	class AL_PD
	{
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"A3_Soft_F"
		};
		units[]=
		{
			"REP_DURANGO_PD",
			"REP_RAPTOR_PD",
			"REP_SAVANNAH_PD",
			"GM_MPD_RAPTOR",
			"GM_MPD_DURANGO",
			"GM_MPD_CHARGER",
			"GM_MPD_CHARGER_SLICKTOP",
			"GM_MPD_EXPLORER",
			"GM_MPD_TAHOE",
			"GM_MPD_TAURUS",
			"GM_MPD_CVPI",
			"GM_SIO_BMW",
			"GM_SIO_CHARGER_SRT",
			"GM_SIO_MUSTANG",
			"GM_SIO_CTSV",
			"GM_SIO_BIKE",
			"GM_SIO_RANGEROVER",
			"GM_SWAT_DURANGO",
			"GM_SWAT_CHARGER",
			"GM_SWAT_EXPLORER",
			"GM_SWAT_RAPTOR",
			"GM_SWAT_TAHOE"
		};
		weapons[]={};
	};
};
class CfgVehicles
{
	class d3s_durango_18_DDPI;
	class REP_DURANGO_PD: d3s_durango_18_DDPI
	{
		author="Repentz";
		scope=2;
		displayName="[PD] Durango";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\durangoPD.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_raptor_PRP_17;
	class REP_RAPTOR_PD: d3s_raptor_PRP_17
	{
		author="Repentz";
		scope=2;
		displayName="[PD] Raptor";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\raptorPD.paa",
			"d3s_tahoe_08\data\Tahoe_wheel.paa",
			"d3s_tahoe_08\data\Tahoe_windows.paa"
		};
	};
	class d3s_nemises_savana_EMS;
	class REP_SAVANNAH_PD: d3s_nemises_savana_EMS
	{
		author="Repentz";
		scope=2;
		displayName="[PD] GMC Savana Prisoner Transport";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\savannahPD.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class GM_MPD_RAPTOR: d3s_raptor_PRP_17
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] Raptor LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\raptor.paa",
			"#(argb,8,8,3)color(0.501961,0.501961,0.501961,1.0,co)",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class GM_MPD_DURANGO: d3s_durango_18_DDPI
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] Durango LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\durango.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_durango_18\data\durango_window.paa"
		};
	};
	class d3s_charger_15_CPP;
	class GM_MPD_CHARGER: d3s_charger_15_CPP
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] Charger LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\charger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_charger_15\data\chr_window.paa"
		};
	};
	class d3s_charger_15_CPST;
	class GM_MPD_CHARGER_SLICKTOP: d3s_charger_15_CPST
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] Charger PB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\charger.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_charger_15\data\chr_window.paa"
		};
	};
	class d3s_nemises_explorer_EMS_13;
	class GM_MPD_EXPLORER: d3s_nemises_explorer_EMS_13
	{
		author="George Matthews";
		scope=2;
		model="\d3s_nemises_explorer_13\EXPLORER_EMS_13";
		displayName="[MPD] Explorer LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\explorer.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_windows.paa"
		};
	};
	class d3s_tahoe_PPV_2;
	class GM_MPD_TAHOE: d3s_tahoe_PPV_2
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] Tahoe LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\tahoe.paa",
			"d3s_cars_core\data\black_co.paa",
			"d3s_tahoe_08\data\tahoe_windows.paa"
		};
	};
	class d3s_nemises_taurus_FPI_10;
	class GM_MPD_TAURUS: d3s_nemises_taurus_FPI_10
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] Ford Taurus LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\taurus.paa",
			"d3s_nemises_taurus_10\data\sho_windows.paa"
		};
	};
	class d3s_crown_98_PD;
	class GM_MPD_CVPI: d3s_crown_98_PD
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD] CVPI LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\cvpi.paa",
			"#(argb,8,8,3)color(0.752941,0.752941,0.752941,1.0,co)",
			"d3s_crown_98\data\cvpi_window.paa"
		};
	};
	class d3s_nemises_f90_18_PD;
	class GM_SIO_BMW: d3s_nemises_f90_18_PD
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SIO] BMW F90 LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\bmw.paa",
			"d3s_nemises_f90_18\data\f90_wheel_co.paa",
			"d3s_nemises_f90_18\data\F90_window.paa"
		};
		maxSpeed=333;
		enginePower=950;
		peakTorque=1200;
	};
	class d3s_srthellcat_15_CPST;
	class GM_SIO_CHARGER_SRT: d3s_srthellcat_15_CPST
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SIO] SRT LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\srt.paa",
			"d3s_nemises_f90_18\data\f90_wheel_co.paa",
			"d3s_nemises_f90_18\data\F90_window.paa"
		};
	};
	class d3s_boss_15_COP;
	class GM_SIO_MUSTANG: d3s_boss_15_COP
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SIO] Mustang LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\mustang.paa",
			"d3s_nemises_f90_18\data\f90_wheel_co.paa",
			"d3s_nemises_f90_18\data\F90_window.paa"
		};
	};
	class d3s_ctsv_16_police;
	class GM_SIO_CTSV: d3s_ctsv_16_police
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SIO] CTSV LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\ctsv.paa",
			"d3s_nemises_f90_18\data\f90_wheel_co.paa",
			"d3s_nemises_f90_18\data\F90_window.paa"
		};
	};
	class d3s_Kawasaki_Ninja_H2R;
	class GM_SIO_BIKE: d3s_Kawasaki_Ninja_H2R
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SIO] Bike LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\bike.paa"
		};
	};
	class d3s_svr_17_COP;
	class GM_SIO_RANGEROVER: d3s_svr_17_COP
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SIO] SVR Sport LBPB";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"AL_Vehicles\PD\data\svr.paa",
			"#(argb,8,8,3)color(0.501961,0.501961,0.501961,1.0,co)",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class d3s_durango_18_UNM;
	class GM_SWAT_DURANGO: d3s_durango_18_UNM
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SWAT] Durango";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"d3s_cars_core\data\black_co.paa",
			"d3s_durango_18\data\durango_wheel.paa",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class d3s_charger_15_CPU;
	class GM_SWAT_CHARGER: d3s_charger_15_CPU
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SWAT] Charger";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"d3s_cars_core\data\black_co.paa",
			"d3s_charger_15\data\chr_wheel2.paa",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class d3s_nemises_explorer_UNM_13;
	class GM_SWAT_EXPLORER: d3s_nemises_explorer_UNM_13
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SWAT] Explorer";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"d3s_cars_core\data\black_co.paa",
			"d3s_nemises_explorer_13\data\exp_wheel_black.paa",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class d3s_raptor_UNM_17;
	class GM_SWAT_RAPTOR: d3s_raptor_UNM_17
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SWAT] Raptor";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"d3s_cars_core\data\black_co.paa",
			"#(argb,8,8,3)color(0.501961,0.501961,0.501961,1.0,co)",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
	class d3s_tahoe_UNM;
	class GM_SWAT_TAHOE: d3s_tahoe_UNM
	{
		author="George Matthews";
		scope=2;
		displayName="[MPD SWAT] Tahoe";
		weapons[]=
		{
			"d3s_airhorn_durango",
			"d3s_airhorn_fcb_priority_durango",
			"d3s_airhorn_fcb_priority2_durango",
			"d3s_airhorn_fcb_priority3_durango",
			"d3s_airhorn_fcb_priority4_durango",
			"d3s_airhorn_fcb_priority5_durango",
			"d3s_airhorn_fcb_priority6_durango",
			"d3s_airhorn_fcb_priority7_durango",
			"d3s_airhorn_fcb_priority8_durango",
			"d3s_airhorn_fcb_priority9_durango",
			"d3s_airhorn_fcb_priority10_durango",
			"d3s_airhorn_fcb_priority11_durango",
			"d3s_airhorn_fcb_priority12_durango",
			"d3s_airhorn_fcb_priority13_durango"
		};
		hiddenSelectionsTextures[]=
		{
			"d3s_cars_core\data\black_co.paa",
			"d3s_tahoe_08\data\gmt_rad_co.paa",
			"d3s_cars_core\data\glass_black.paa"
		};
	};
};
